package outputVariablesEditor;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.inject.Inject;

import com.fuzzylite.Engine;
import com.fuzzylite.defuzzifier.Bisector;
import com.fuzzylite.defuzzifier.Centroid;
import com.fuzzylite.defuzzifier.LargestOfMaximum;
import com.fuzzylite.defuzzifier.MeanOfMaximum;
import com.fuzzylite.defuzzifier.SmallestOfMaximum;
import com.fuzzylite.defuzzifier.WeightedAverage;
import com.fuzzylite.defuzzifier.WeightedSum;
import com.fuzzylite.norm.s.AlgebraicSum;
import com.fuzzylite.norm.s.BoundedSum;
import com.fuzzylite.norm.s.DrasticSum;
import com.fuzzylite.norm.s.EinsteinSum;
import com.fuzzylite.norm.s.HamacherSum;
import com.fuzzylite.norm.s.Maximum;
import com.fuzzylite.norm.s.NilpotentMaximum;
import com.fuzzylite.norm.s.NormalizedSum;
import com.fuzzylite.term.Term;
import com.fuzzylite.term.Triangle;
import com.fuzzylite.variable.OutputVariable;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import outputVariable.OutputVariablePresenter;
import ressources.Util;

public class OutputVariablesEditorPresenter implements Initializable {

	@FXML
	AreaChart<Double, Double> outPutVarChart;

	@FXML
	NumberAxis xAxis;

	@FXML
	NumberAxis yAxis;

	@FXML
	TextField defaultValueTextField;

	@FXML
	TextField resolutionOfIntegralDefuzzifierTextField;

	@FXML
	TextField varNameTextField;

	@FXML
	TextField minimumTextField;

	@FXML
	TextField maximumTextField;

	@FXML
	TextField outputTextField;

	@FXML
	ComboBox<String> accumulationMethod;

	@FXML
	ComboBox<String> defuzzifyMethod;

	@FXML
	TextField termNameTextField;

	@FXML
	Button addTermButton;

	@FXML
	Button removeTermButton;

	@FXML
	ComboBox<String> termFormComboBox;

	@FXML
	TextField heightTextField;

	@FXML
	HBox parameterHBox;

	@FXML
	Button doneButton;

	@FXML
	Button cancelButton;

	@FXML
	Button doneEditingTermButton;

	@Inject
	Engine engine;

	private OutputVariablePresenter presenter;

	private boolean changed = false;

	private OutputVariable outputVariable;

	// List with all possible defuzzification methods
	private ObservableList<String> defuzzifyMethods = FXCollections.observableArrayList("Bisector", "Centroid",
			"LargestOfMaximum", "MeanOfMaximum", "SmallestOfMaximum", "WeightedAverage", "WeightedSum");

	// List with all possible accumulation methods
	private ObservableList<String> accumulationMethods = FXCollections.observableArrayList("AlgebraicSum", "BoundedSum",
			"DrasticSum", "EinsteinSum", "HamacherSum", "Maximum", "NilpotentMaximum", "NormalizedSum");

	private XYChart.Series activatedSeries;

	private ObservableList<String> termFroms = FXCollections.observableArrayList("Triangle", "Trapezoid", "Ramp");

	private Term activatedTerm;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}

	/**
	 * Draw the AreaChart out of the Terms of the OutputVariable
	 *
	 * @param OutputVariable
	 *            var
	 */
	private void drawAreaChart(OutputVariable var) {
		outPutVarChart.getData().clear();
		for (Term term : var.getTerms()) {
			ArrayList<Double> parameters = Util.extractTermParameter(term);

			XYChart.Series series = new XYChart.Series();

			if (Util.getTermForm(term) == "Ramp") {
				series.getData().add(new XYChart.Data(parameters.get(0), 0.0));
				series.getData().add(new XYChart.Data(parameters.get(1), 1.0));
				series.getData().add(new XYChart.Data(engine.getOutputVariable(0).getMaximum(), 1.0));
			} else {
				for (int i = 0; i < parameters.size(); i++) {
					if (i == 0) {
						series.getData().add(new XYChart.Data(parameters.get(i), 0.0));
					} else if (i == parameters.size() - 1) {
						series.getData().add(new XYChart.Data(parameters.get(i), 0.0));
					} else {
						series.getData().add(new XYChart.Data(parameters.get(i), 1.0));
					}
				}
			}

			series.setName(term.getName());
			outPutVarChart.getData().add(series);
		}

		for (XYChart.Series series : outPutVarChart.getData()) {

			// add MouseEventHandler to each series of the terms
			series.getNode().addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent event) {

					// delete TextFields of the term
					parameterHBox.getChildren().clear();
					HBox hbox = new HBox();
					hbox.setHgrow(hbox, Priority.ALWAYS);
					parameterHBox.getChildren().addAll(hbox);

					removeTermButton.setDisable(false);
					doneEditingTermButton.setVisible(true);
					termFormComboBox.setDisable(false);

					for (XYChart.Series series : outPutVarChart.getData()) {
						series.getNode().setEffect(null);
					}

					activatedSeries = series;

					// set shadow on clicked series
					series.getNode().setEffect(new DropShadow());

					activatedTerm = Util.findTermInEngineFromSeries(series, outputVariable);

					ArrayList<Double> parameters = Util.extractTermParameter(activatedTerm);

					// create and add a TextField for each parameter of the Term
					for (int i = 0; i < parameters.size(); i++) {
						TextField textField = new TextField();
						textField.setAlignment(Pos.CENTER);
						textField.setText(Double.toString(parameters.get(i)));
						textField.setPrefWidth(75);
						textField.setOnKeyPressed(new EventHandler<KeyEvent>() {

							@Override
							public void handle(KeyEvent event) {
								if (event.getCode().equals(KeyCode.ENTER)) {
									editTerm();
								}
							}
						});
						HBox h = new HBox();
						h.setHgrow(h, Priority.ALWAYS);
						parameterHBox.getChildren().addAll(textField, h);
					}

					heightTextField.setVisible(true);

					// set up GUI items after click on a series
					termNameTextField.setText(activatedTerm.getName());
					termFormComboBox.setValue(Util.getTermForm(activatedTerm));
					heightTextField.setText(Double.toString(activatedTerm.getHeight()));

					termFormComboBox.valueProperty().addListener(new ChangeListener<String>() {

						@Override
						public void changed(ObservableValue<? extends String> observable, String oldValue,
								String newValue) {

							if (oldValue != "") {
								switch (newValue) {
								case "Triangle":
									if (activatedTerm != null) {
										Term activatedTerm = Util.findTermInEngineFromSeries(activatedSeries,
												outputVariable);
										Term term = Util.transformTermToTriangle(outputVariable, activatedTerm,
												Util.getTermForm(activatedTerm));
										if (term != null) {
											outputVariable.removeTerm(activatedTerm);
											outputVariable.addTerm(term);
											refresh();
											deselectTerm();
										}
									}
									break;
								case "Trapezoid":
									if (activatedTerm != null) {
										Term activatedTerm = Util.findTermInEngineFromSeries(activatedSeries,
												outputVariable);
										Term term = Util.transformTermToTrapezoid(outputVariable, activatedTerm,
												Util.getTermForm(activatedTerm));
										if (term != null) {
											outputVariable.removeTerm(activatedTerm);
											outputVariable.addTerm(term);
											refresh();
											deselectTerm();
										}
									}
									break;
								case "Ramp":
									if (activatedTerm != null) {
										Term activatedTerm = Util.findTermInEngineFromSeries(activatedSeries,
												outputVariable);
										Term term = Util.transformTermToRamp(outputVariable, activatedTerm,
												Util.getTermForm(activatedTerm));
										if (term != null) {
											outputVariable.removeTerm(activatedTerm);
											outputVariable.addTerm(term);
											refresh();
											deselectTerm();
										}

									}
									break;
								}
							}

						}

					});

				}

			});
		}
	}

	/**
	 * Sets up range and tick unit for xAxis and yAxis
	 */
	private void setUpAreaChart() {

		// set the maximum range for the yAxis
		yAxis.setAutoRanging(false);
		yAxis.setUpperBound(1.0);

		// set the tick unit for the yAxis
		yAxis.setTickUnit(0.25);

		// set the maximum range for the xAxis
		xAxis.setAutoRanging(false);
		xAxis.setUpperBound(outputVariable.getMaximum());
		xAxis.setLowerBound(outputVariable.getMinimum());

		// set the tick unit for the xAxis
		xAxis.setTickUnit(1.0);
	}

	/**
	 * Initializes all ComboBoxes of the View
	 */
	private void setComboBoxes() {
		defuzzifyMethod.setItems(defuzzifyMethods);
		defuzzifyMethod.setValue(Util.extractDefuzzificationMethod(outputVariable.getDefuzzifier()));
		accumulationMethod.setItems(accumulationMethods);
		accumulationMethod.setValue(Util.extractNameFormNorm(outputVariable.fuzzyOutput().getAccumulation()));
		termFormComboBox.setItems(termFroms);
	}

	public void setUpAllItems() {
		varNameTextField.setText(outputVariable.getName());
		if (outputVariable.getName().trim().isEmpty() || outputVariable.getName() == null) {
			doneButton.setDisable(true);
		}

		// Checks if name of inputVariable does not already exist, is null or
		// contains only spaces
		varNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue.trim().isEmpty() || newValue == null) {
				doneButton.setDisable(true);
			} else if (newValue.equals(outputVariable.getName())) {
				doneButton.setDisable(false);
			} else if (!newValue.equals(outputVariable.getName())) {
				if (!Util.checkIfVariableAlreadyExists(newValue, engine)) {
					doneButton.setDisable(false);
				} else {
					doneButton.setDisable(true);
				}
			} else {
				doneButton.setDisable(false);
			}
		});

		defaultValueTextField.setText(Double.toString(outputVariable.getDefaultValue()));
		minimumTextField.setText(Double.toString(outputVariable.getMinimum()));
		maximumTextField.setText(Double.toString(outputVariable.getMaximum()));
		outputTextField.setText(Double.toString(outputVariable.getOutputValue()));
		setComboBoxes();
	}

	/**
	 * Refrehes all GUI items
	 */
	public void refresh() {
		setUpAllItems();
		setUpAreaChart();
		drawAreaChart(outputVariable);
	}

	/**
	 * Reads the Value of the varNameTextField on Enter pressed and changes the
	 * name of the OutputVariable in the Engine
	 *
	 * @param ke
	 */
	public void varNameTextFieldChange(KeyEvent ke) {
		if (ke.getCode().equals(KeyCode.ENTER)) {
			outputVariable.setName(varNameTextField.getText());
			refresh();
		}
	}

	/**
	 * Reads the Value of the maximumTextField on Enter pressed and changes the
	 * maximum of the OutputVariable in the Engine
	 *
	 * @param ke
	 */
	public void maximumTextFieldChange(KeyEvent ke) {
		if (ke.getCode().equals(KeyCode.ENTER)) {
			outputVariable.setMaximum(Double.parseDouble(maximumTextField.getText()));
			xAxis.setUpperBound(outputVariable.getMaximum());
			refresh();
		}
	}

	/**
	 * Reads the Value of the minimumTextField on Enter pressed and changes the
	 * minimum of the OutputVariable in the Engine
	 *
	 * @param ke
	 */
	public void minimumTextFieldChange(KeyEvent ke) {
		if (ke.getCode().equals(KeyCode.ENTER)) {
			outputVariable.setMinimum(Double.parseDouble(minimumTextField.getText()));
			xAxis.setLowerBound(engine.getInputVariable(0).getMinimum());
			refresh();
		}
	}

	/**
	 * Reads the Value of the defaultTextField on Enter pressed and changes the
	 * default value of the OutputVariable in the Engine
	 *
	 * @param ke
	 */
	public void defaultTextFieldChange(KeyEvent ke) {
		if (ke.getCode().equals(KeyCode.ENTER)) {
			outputVariable.setDefaultValue(Double.parseDouble(defaultValueTextField.getText()));
			refresh();
		}
	}

	/**
	 * Takes the value of the defuzzifyMethod ComboBox and sets the
	 * defuzzification method method of the current engine
	 *
	 * @param event
	 */
	public void defuzzifyMethodComboChanged(ActionEvent event) {

		String selection = defuzzifyMethod.getValue();

		switch (selection) {
		case "Bisector":
			outputVariable.setDefuzzifier(new Bisector());
			break;
		case "Centroid":
			outputVariable.setDefuzzifier(new Centroid());
			break;
		case "LargestOfMaximum":
			outputVariable.setDefuzzifier(new LargestOfMaximum());
			break;
		case "MeanOfMaximum":
			outputVariable.setDefuzzifier(new MeanOfMaximum());
			break;
		case "SmallestOfMaximum":
			outputVariable.setDefuzzifier(new SmallestOfMaximum());
			break;
		case "WeightedAverage":
			outputVariable.setDefuzzifier(new WeightedAverage());
			break;
		case "NilpotentMinimum":
			outputVariable.setDefuzzifier(new WeightedSum());
			break;
		}
	}

	/**
	 * Takes the value of the disjuctionMethodComboBox and sets the disjunction
	 * method of the current engine
	 *
	 * @param event
	 */
	public void accumulationMethodComboChanged(ActionEvent event) {

		String selection = accumulationMethod.getValue();

		switch (selection) {
		case "AlgebraicSum":
			outputVariable.fuzzyOutput().setAccumulation((new AlgebraicSum()));
			break;
		case "BoundedSum":
			outputVariable.fuzzyOutput().setAccumulation((new BoundedSum()));
			break;
		case "DrasticSum":
			outputVariable.fuzzyOutput().setAccumulation((new DrasticSum()));
			break;
		case "EinsteinSum":
			outputVariable.fuzzyOutput().setAccumulation((new EinsteinSum()));
			break;
		case "HamacherSum":
			outputVariable.fuzzyOutput().setAccumulation((new HamacherSum()));
			break;
		case "Maximum":
			outputVariable.fuzzyOutput().setAccumulation((new Maximum()));
			break;
		case "NilpotentSum":
			outputVariable.fuzzyOutput().setAccumulation((new NilpotentMaximum()));
			break;
		case "NormalizedSum":
			outputVariable.fuzzyOutput().setAccumulation((new NormalizedSum()));
			break;
		}
	}

	/**
	 *
	 *
	 * @param ke
	 */
	public void heightTextFieldChange(KeyEvent ke) {
		Term term = null;
		for (Term helpTerm : outputVariable.getTerms()) {
			if (activatedSeries != null) {
				if (helpTerm.getName() == activatedSeries.getName()) {
					term = helpTerm;
				}

				term.setHeight(Double.parseDouble(heightTextField.getText()));
				refresh();
			} else {
				return;
			}
		}
	}

	public void handleDoneButtonAction(ActionEvent event) {
		updateOutputVariable();
		editTerm();
		Stage stage = (Stage) doneButton.getScene().getWindow();
		changed = true;
		stage.close();
	}

	private void updateOutputVariable() {
		outputVariable.setName(varNameTextField.getText());
		outputVariable.setMaximum(Double.parseDouble(maximumTextField.getText()));
		outputVariable.setMinimum(Double.parseDouble(minimumTextField.getText()));
		outputVariable.setDefaultValue(Double.parseDouble(defaultValueTextField.getText()));
	}

	public void handleCancelButtonAction(ActionEvent event) {
		Stage stage = (Stage) cancelButton.getScene().getWindow();
		stage.close();
	}

	public void onAddTermButtonPressed(ActionEvent event) {
		outputVariable.addTerm(new Triangle("", 0.000, 1.000, 2.000));
		refresh();
	}

	/**
	 * Button Handler - Remove the activated Term out of the engine and re-draws
	 * the Chart
	 *
	 * @param event
	 */
	public void onRemoveTermButtonPressed(ActionEvent event) {
		Term activatedTerm = Util.findTermInEngineFromSeries(activatedSeries, outputVariable);
		outputVariable.removeTerm(activatedTerm);
		deselectTerm();
		refresh();
	}

	/**
	 * Sets the presenter
	 *
	 * @param presenter
	 */
	public void setPresenter(OutputVariablePresenter presenter) {
		this.presenter = presenter;
	}

	/**
	 * Return the Variable changed
	 *
	 * @return boolean changed
	 */
	public boolean getChanged() {
		return changed;
	}

	/**
	 * Sets the outputVariable of the presenter
	 *
	 * @param outputVariable
	 */
	public void setOutputVariable(OutputVariable outputVariable) {
		this.outputVariable = outputVariable;
		setUpAllItems();
		setUpAreaChart();
		drawAreaChart(outputVariable);
	}

	/**
	 * Returns the outputVariable of the presenter
	 *
	 * @return OutputVariable outputVariable
	 */
	public OutputVariable getOutputVariable() {
		return outputVariable;
	}

	/**
	 * ButtonListener for DoneEditingButton
	 *
	 * @param event
	 */
	public void onDoneEditingTermButtonPressed(ActionEvent event) {
		editTerm();
		deselectTerm();
	}

	public void doneEditingTermNameTextField(ActionEvent event) {
		Term term = Util.findTermInEngineFromSeries(activatedSeries, outputVariable);
		term.setName(termNameTextField.getText());
		refresh();
	}

	/**
	 * Function that is called when the parameters of a term a edited
	 */
	private void editTerm() {
		String parameters = "";

		int i = 1;
		while (i < parameterHBox.getChildren().size()) {
			TextField textField = (TextField) parameterHBox.getChildren().get(i);
			parameters += textField.getText() + " ";

			i = i + 2;
		}

		if (activatedTerm != null) {
			outputVariable.getTerm(activatedTerm.getName()).configure(parameters);
			outputVariable.getTerm(activatedTerm.getName()).setName(termNameTextField.getText());
			outputVariable.getTerm(activatedTerm.getName()).setHeight(Double.parseDouble(heightTextField.getText()));
		}

		refresh();
	}

	private void deselectTerm() {
		activatedSeries = null;
		for (XYChart.Series series : outPutVarChart.getData()) {
			series.getNode().setEffect(null);
		}
		parameterHBox.getChildren().clear();
		heightTextField.setVisible(false);
		doneEditingTermButton.setVisible(false);
		termNameTextField.setText("");
		termFormComboBox.setDisable(true);
	}

}
