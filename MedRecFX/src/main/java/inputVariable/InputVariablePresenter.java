package inputVariable;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.inject.Inject;

import com.fuzzylite.Engine;
import com.fuzzylite.term.Term;
import com.fuzzylite.variable.InputVariable;

import inputVariablesEditor.InputVariablesEditorPresenter;
import inputVariablesEditor.InputVariablesEditorView;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import ressources.Util;

public class InputVariablePresenter implements Initializable {

	@FXML
	Label inputVarName;

	@FXML
	Label varActivationLabel;

	@FXML
	AreaChart<Double, Double> inPutVarChart;

	@FXML
	NumberAxis xAxis;

	@FXML
	NumberAxis yAxis;

	@FXML
	TextField inputValueTextField;

	@FXML
	TextField minimumTextField;

	@FXML
	TextField maximumTextField;

	@Inject
	Engine engine;

	private InputVariable inputVariable;

	private InputVariable tmpInput;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}

	/**
	 * Set inputVariable and refresh the UI
	 *
	 * @param inputVariable
	 */
	public void setInputVariable(InputVariable inputVariable) {
		this.inputVariable = inputVariable;
		refresh();
	}

	/**
	 * Get inputVariable
	 *
	 * @return InputVariable inputVariable
	 */
	public InputVariable getInputVariable() {
		return inputVariable;
	}

	/**
	 * Draws the AreaChart out of the Terms of the InputVariable
	 *
	 * @param InputVariable
	 *            var
	 */
	private void drawAreaChart(InputVariable var) {
		inPutVarChart.getData().clear();
		for (Term term : var.getTerms()) {
			ArrayList<Double> parameters = Util.extractTermParameter(term);

			// Create new Chart Sewries
			XYChart.Series series = new XYChart.Series();

			// Add data to AreaChart and draw the chart
			if (Util.getTermForm(term) == "Ramp") {
				series.getData().add(new XYChart.Data(parameters.get(0), 0.0));
				series.getData().add(new XYChart.Data(parameters.get(1), 1.0));
				series.getData().add(new XYChart.Data(inputVariable.getMaximum(), 1.0));
			} else {
				for (int i = 0; i < parameters.size(); i++) {
					if (i == 0) {
						series.getData().add(new XYChart.Data(parameters.get(i), 0.0));
					} else if (i == parameters.size() - 1) {
						series.getData().add(new XYChart.Data(parameters.get(i), 0.0));
					} else {
						series.getData().add(new XYChart.Data(parameters.get(i), 1.0));
					}
				}
			}

			series.setName(term.getName());
			inPutVarChart.getData().add(series);
		}
	}

	/**
	 * Sets up range and tick unit for xAxis and yAxis
	 */
	private void setUpAreaChart() {

		// set the maximum range for the yAxis
		yAxis.setAutoRanging(false);
		yAxis.setUpperBound(1.0);

		// set the tick unit for the yAxis
		yAxis.setTickUnit(0.25);

		// set the maximum range for the xAxis
		xAxis.setAutoRanging(false);
		xAxis.setUpperBound(inputVariable.getMaximum());
		xAxis.setLowerBound(inputVariable.getMinimum());

		// set the tick unit for the xAxis
		xAxis.setTickUnit(1.0);
	}

	/**
	 * Initializes all TextFields of the View
	 */
	private void setTextFields() {
		minimumTextField.setText(Double.toString(inputVariable.getMinimum()));
		inputValueTextField.setText(Double.toString(inputVariable.getInputValue()));
		maximumTextField.setText(Double.toString(inputVariable.getMaximum()));
	}

	/**
	 * Is called when the EditButton is clicked
	 *
	 * @param event
	 * @throws IOException
	 */
	public void onEditButtonPressed(ActionEvent event) throws IOException {

		javafx.application.Platform.runLater(new Runnable() {

			@Override
			public void run() {
				Stage stage = new Stage();
				InputVariablesEditorView view = new InputVariablesEditorView();
				InputVariablesEditorPresenter presenter = (InputVariablesEditorPresenter) view.getPresenter();
				try {
					tmpInput = inputVariable.clone();
				} catch (CloneNotSupportedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				presenter.setInputVariable(tmpInput);

				// Create new window
				Scene scene = new Scene(view.getView());
				stage.setScene(scene);
				stage.setTitle("Input Variable Editor - " + inputVariable.getName());
				// scene.getStylesheets().add("OutputVariablesEditor.css");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.show();

				// Perform action when Window is closed
				stage.setOnHidden(new EventHandler<WindowEvent>() {

					@Override
					public void handle(WindowEvent event) {
						tmpInput = new InputVariable();
						if (presenter.getChanged()) {
							int index = Util.getIndexOfInputVariableInEngine(inputVariable, engine);
							inputVariable = presenter.getInputVariable();
							updateInputVariableInEngine(index, inputVariable);
							refresh();
						}
					}
				});

			}
		});
	}

	/**
	 * Refreshes all UI elements of the inputvariable
	 */
	public void refresh() {
		inputVarName.setText(inputVariable.getName());
		setUpAreaChart();
		drawAreaChart(inputVariable);
		setTextFields();
	}

	/**
	 * Reads the Value of the maximumTextField on Enter pressed and changes the
	 * maximum of the InputVariable in the Engine
	 *
	 * @param ke
	 */
	public void maximumTextFieldChange(KeyEvent ke) {
		if (ke.getCode().equals(KeyCode.ENTER)) {
			inputVariable.setMaximum(Double.parseDouble(maximumTextField.getText()));
			xAxis.setUpperBound(inputVariable.getMaximum());
			refresh();
		}
	}

	/**
	 * Reads the Value of the minimumTextField on Enter pressed and changes the
	 * minimum of the InputVariable in the Engine
	 *
	 * @param ke
	 */
	public void minimumTextFieldChange(KeyEvent ke) {
		if (ke.getCode().equals(KeyCode.ENTER)) {
			inputVariable.setMinimum(Double.parseDouble(minimumTextField.getText()));
			xAxis.setLowerBound(inputVariable.getMinimum());
			refresh();
		}
	}

	/**
	 * Update the InputVariable in the engine
	 *
	 * @param inputVariable
	 */
	private void updateInputVariableInEngine(int index, InputVariable inputVariable) {
		engine.getOutputVariable(index).setTerms(inputVariable.getTerms());
		engine.getOutputVariable(index).setName(inputVariable.getName());
		engine.getOutputVariable(index).setMinimum(inputVariable.getMinimum());
		engine.getOutputVariable(index).setMaximum(inputVariable.getMaximum());
	}

}
