package ruleEditor;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javax.inject.Inject;
import javax.security.auth.callback.Callback;

import com.fuzzylite.Engine;
import com.fuzzylite.norm.Norm;
import com.fuzzylite.norm.SNorm;
import com.fuzzylite.norm.TNorm;
import com.fuzzylite.norm.s.AlgebraicSum;
import com.fuzzylite.norm.s.BoundedSum;
import com.fuzzylite.norm.s.DrasticSum;
import com.fuzzylite.norm.s.EinsteinSum;
import com.fuzzylite.norm.s.HamacherSum;
import com.fuzzylite.norm.s.Maximum;
import com.fuzzylite.norm.s.NilpotentMaximum;
import com.fuzzylite.norm.s.NormalizedSum;
import com.fuzzylite.norm.t.AlgebraicProduct;
import com.fuzzylite.norm.t.BoundedDifference;
import com.fuzzylite.norm.t.DrasticProduct;
import com.fuzzylite.norm.t.EinsteinProduct;
import com.fuzzylite.norm.t.HamacherProduct;
import com.fuzzylite.norm.t.Minimum;
import com.fuzzylite.norm.t.NilpotentMinimum;
import com.fuzzylite.rule.Rule;
import com.fuzzylite.rule.RuleBlock;
import com.fuzzylite.variable.OutputVariable;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableDoubleValue;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Cell;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ToolBar;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Region;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import outputVariablesEditor.OutputVariablesEditorPresenter;
import outputVariablesEditor.OutputVariablesEditorView;
import ressources.Util;
import ruleBlockEditor.RuleBlockEditorPresenter;
import ruleBlockEditor.RuleBlockEditorView;

public class RuleEditorPresenter implements Initializable {

	@FXML
	Label ruleBlockNameLabel;

	@FXML
	ComboBox<String> conjunctionMethod;

	@FXML
	ComboBox<String> disjunctionMethod;

	@FXML
	ComboBox<String> activationMethod;

	@FXML
	Label numberActivatedRulesLabel;

	@FXML
	Button editRuleBlockButton;

	@FXML
	ToolBar ruleEditorToolBar;

	@FXML
	TableView<Rule> rulesTableView;

	@FXML
	TableColumn<Rule, Integer> ruleNumberTableColumn;

	@FXML
	TableColumn<Rule, String> ruleTableColumn;

	@FXML
	TableColumn<Rule, Double> activationDegreeTableColumn;

	@Inject
	Engine engine;

	private ObservableList<String> conjunctionMethods = FXCollections.observableArrayList("AlgebraicProduct",
			"BoundedDifference", "DrasticProduct", "EinsteinProduct", "HamacherProduct", "Minimum", "NilpotentMinimum");

	private ObservableList<String> disjunctionMethods = FXCollections.observableArrayList("AlgebraicSum", "BoundedSum",
			"DrasticSum", "EinsteinSum", "HamacherSum", "Maximum", "NilpotentMaximum", "NormalizedSum");

	private ObservableList<String> activationMethods = FXCollections.observableArrayList("AlgebraicProduct",
			"BoundedDifference", "DrasticProduct", "EinsteinProduct", "HamacherProduct", "Minimum", "NilpotentMinimum");

	private ObservableList<Rule> rules = FXCollections.observableArrayList();

	private RuleBlock tmpRuleBlock;

	private Engine tmpEngine;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		ruleBlockNameLabel.setText("Regelblock: ");
		numberActivatedRulesLabel.setText("0/0");

	}

	private void setComboBoxes() {

		// Set conjunctionMethod-comboBox
		conjunctionMethod.setItems(conjunctionMethods);
		conjunctionMethod.setValue(Util.extractNameFormNorm(engine.getRuleBlock(0).getConjunction()));

		// Set disjunctionMethod-comboBox
		disjunctionMethod.setItems(disjunctionMethods);
		disjunctionMethod.setValue(Util.extractNameFormNorm(engine.getRuleBlock(0).getDisjunction()));

		// Set activationMethod-comboBox
		activationMethod.setItems(activationMethods);
		activationMethod.setValue(Util.extractNameFormNorm(engine.getRuleBlock(0).getActivation()));
	}

	/**
	 * Adds content to all TableColumns of the rulesTableView
	 */
	private void setTableViewItems() {
		rulesTableView.setItems(rules);

		// Add content to first column
		ruleTableColumn.setCellValueFactory(cellData -> {
			String rule = cellData.getValue().getText();
			String outputString = "";
			String help = "";
			String rest = rule;
			if (rule.length() >= (int) ruleTableColumn.getWidth() / 7) {
				while (rest.length() >= (int) ruleTableColumn.getWidth() / 7) {
					if (rest.indexOf("then") < 170) {
						help = rest.substring(0, rest.indexOf("then"));
						rest = rest.substring(rest.indexOf("then"));
						outputString += help + "\n";
					} else if (rest.indexOf(" and ", 100) < 170) {
						help = rest.substring(0, rest.indexOf(" and ", 100));
						rest = rest.substring(rest.indexOf(" and ", 100));
						outputString += help + "\n";
					} else if (rest.indexOf(" or ", 100) < 170) {
						help = rest.substring(0, rest.indexOf(" or ", 100));
						rest = rest.substring(rest.indexOf(" or ", 100));
						outputString += help + "\n";
					} else if (rest.indexOf(" is ", 120) < 170) {
						help = rest.substring(0, rest.indexOf(" is ", 120));
						rest = rest.substring(rest.indexOf(" is ", 120));
						outputString += help + "\n";
					} else if (rest.indexOf(" ", 130) < 170) {
						help = rest.substring(0, rest.indexOf(" ", 130));
						rest = rest.substring(rest.indexOf(" ", 130));
						outputString += help + "\n";
					}
				}
				outputString += rest;
			} else {
				outputString = rule;
			}
			ObservableValue<String> obsOutputString = new ReadOnlyObjectWrapper<String>(outputString);
			return obsOutputString;
		});

		ruleTableColumn.setSortable(false);

		// Add content to second column
		activationDegreeTableColumn.setCellValueFactory(cellData -> {
			double actDegree = cellData.getValue().activationDegree(engine.getRuleBlock(0).getConjunction(),
					engine.getRuleBlock(0).getDisjunction());

			// if(Double.isNaN(actDegree)) {
			// actDegree = 0.0;
			// }
			ObservableValue<Double> obsActDegree = new ReadOnlyObjectWrapper<Double>(actDegree);
			return obsActDegree;
		});

		// Add content to third column
		ruleNumberTableColumn.setCellValueFactory(cellData -> {
			String rule = cellData.getValue().getText();
			for (int i = 0; i < engine.getRuleBlock(0).getRules().size(); i++) {
				if (rule.equals(engine.getRuleBlock(0).getRule(i).getText())) {
					ObservableValue<Integer> obsIndex = new ReadOnlyObjectWrapper<Integer>(i + 1);
					return obsIndex;
				}
			}
			ObservableValue<Integer> index = new ReadOnlyObjectWrapper<Integer>(0);
			return index;
		});

		rulesTableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

		//Center the content of the first and third tableColumn
		rulesTableView.getColumns().get(0).setStyle("-fx-alignment: CENTER;");
		rulesTableView.getColumns().get(2).setStyle("-fx-alignment: CENTER;");

	}

	/**
	 * Takes the value of the conjuctionMethodComboBox and sets the conjunction
	 * method of the current engine
	 *
	 * @param event
	 */
	public void conjunctionMethodComboChanged(ActionEvent event) {

		String selection = conjunctionMethod.getValue();

		switch (selection) {
		case "AlgebraicProduct":
			engine.getRuleBlock(0).setConjunction(new AlgebraicProduct());
			break;
		case "BoundedDifference":
			engine.getRuleBlock(0).setConjunction(new BoundedDifference());
			break;
		case "DrasticProduct":
			engine.getRuleBlock(0).setConjunction(new DrasticProduct());
			break;
		case "EinsteinProduct":
			engine.getRuleBlock(0).setConjunction(new EinsteinProduct());
			break;
		case "HamacherProduct":
			engine.getRuleBlock(0).setConjunction(new HamacherProduct());
			break;
		case "Minimum":
			engine.getRuleBlock(0).setConjunction(new Minimum());
			break;
		case "NilpotentMinimum":
			engine.getRuleBlock(0).setConjunction(new NilpotentMinimum());
			break;
		}
	}

	/**
	 * Takes the value of the disjuctionMethodComboBox and sets the disjunction
	 * method of the current engine
	 *
	 * @param event
	 */
	public void disjunctionMethodComboChanged(ActionEvent event) {

		String selection = disjunctionMethod.getValue();

		switch (selection) {
		case "AlgebraicSum":
			engine.getRuleBlock(0).setDisjunction(new AlgebraicSum());
			break;
		case "BoundedSum":
			engine.getRuleBlock(0).setDisjunction(new BoundedSum());
			break;
		case "DrasticSum":
			engine.getRuleBlock(0).setDisjunction(new DrasticSum());
			break;
		case "EinsteinSum":
			engine.getRuleBlock(0).setDisjunction(new EinsteinSum());
			break;
		case "HamacherSum":
			engine.getRuleBlock(0).setDisjunction(new HamacherSum());
			break;
		case "Maximum":
			engine.getRuleBlock(0).setDisjunction(new Maximum());
			break;
		case "NilpotentSum":
			engine.getRuleBlock(0).setDisjunction(new NilpotentMaximum());
			break;
		case "NormalizedSum":
			engine.getRuleBlock(0).setDisjunction(new NormalizedSum());
			break;
		}
	}

	/**
	 * Takes the value of the activationMethodComboBox and sets the activation
	 * method of the current engine
	 *
	 * @param event
	 */
	public void activationMethodComboChanged(ActionEvent event) {

		String selection = activationMethod.getValue();

		switch (selection) {
		case "AlgebraicProduct":
			engine.getRuleBlock(0).setActivation(new AlgebraicProduct());
			break;
		case "BoundedDifference":
			engine.getRuleBlock(0).setActivation(new BoundedDifference());
			break;
		case "DrasticProduct":
			engine.getRuleBlock(0).setActivation(new DrasticProduct());
			break;
		case "EinsteinProduct":
			engine.getRuleBlock(0).setActivation(new EinsteinProduct());
			break;
		case "HamacherProduct":
			engine.getRuleBlock(0).setActivation(new HamacherProduct());
			break;
		case "Minimum":
			engine.getRuleBlock(0).setActivation(new Minimum());
			break;
		case "NilpotentMinimum":
			engine.getRuleBlock(0).setActivation(new NilpotentMinimum());
			break;
		}
	}

	/**
	 * Counts the number of activated rules
	 *
	 * @return int number of activated rules
	 */
	private int countActivatedRules() {
		int count = 0;
		for (int i = 0; i < engine.getRuleBlock(0).getRules().size(); i++) {
			if (engine.getRuleBlock(0).getRules().get(i).activationDegree(engine.getRuleBlock(0).getConjunction(),
					engine.getRuleBlock(0).getDisjunction()) > 0.0) {
				count++;
			}
		}
		return count;
	}

	/**
	 * Refreshes the NumberOfRulesLabel
	 */
	private void refreshNumberOfRulesLabel() {
		numberActivatedRulesLabel.setText(countActivatedRules() + "/" + engine.getRuleBlock(0).numberOfRules());
	}

	/**
	 * Adds all rules of the current engine to the ObservableArrayList used for
	 * the TableView
	 */
	private void addElementsFromEngineToObservableArrayList() {
		rules.clear();
		for (int i = 0; i < engine.getRuleBlock(0).getRules().size(); i++) {
			rules.add(engine.getRuleBlock(0).getRule(i));
		}
	}

	/**
	 * Refresh all UI elements of the RuleEditor
	 */
	public void refresh() {

		ruleBlockNameLabel.setText("Regelblock: " + engine.getName());
		setComboBoxes();
		addElementsFromEngineToObservableArrayList();
		setTableViewItems();
		refreshNumberOfRulesLabel();

	}

	/**
	 * Open the new View when the EditButton is clicked
	 *
	 * @param event
	 * @throws IOException
	 */
	public void onEditButtonPressed(ActionEvent event) throws IOException {

		javafx.application.Platform.runLater(new Runnable() {

			@Override
			public void run() {
				Stage stage = new Stage();
				RuleBlockEditorView view = new RuleBlockEditorView();
				RuleBlockEditorPresenter presenter = (RuleBlockEditorPresenter) view.getPresenter();
				/*
				try {
					tmpEngine = engine.clone();
					tmpRuleBlock = tmpEngine.getRuleBlock(0);
				} catch (CloneNotSupportedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				*/

				tmpRuleBlock = engine.getRuleBlock(0);

				presenter.setRuleBlock(tmpRuleBlock);
				Scene scene = new Scene(view.getView());
				stage.setScene(scene);
				stage.setTitle("Rule Block Editor");
				scene.getStylesheets().add("RuleBlockEditor.css");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.show();
				stage.setOnHidden(new EventHandler<WindowEvent>() {

					@Override
					public void handle(WindowEvent event) {
						//tmpEngine = new Engine()
						tmpRuleBlock = new RuleBlock();
						if (presenter.isChanged()) {
							RuleBlock helpRuleBlock = presenter.getRuleBlock();
							updateRuleBlock(helpRuleBlock);
							refresh();
						}
					}
				});

			}
		});
	}

	/**
	 * Updates the RuleBlock in the engine to the given RuleBlock
	 * @param ruleBlock
	 */
	private void updateRuleBlock(RuleBlock ruleBlock){
		engine.getRuleBlock(0).setName(ruleBlock.getName());
		engine.getRuleBlock(0).setActivation(ruleBlock.getActivation());
		engine.getRuleBlock(0).setConjunction(ruleBlock.getConjunction());
		engine.getRuleBlock(0).setDisjunction(ruleBlock.getDisjunction());
		engine.getRuleBlock(0).setRules(ruleBlock.getRules());
	}

}
