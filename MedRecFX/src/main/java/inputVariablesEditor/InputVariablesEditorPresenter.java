package inputVariablesEditor;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.inject.Inject;

import com.fuzzylite.Engine;
import com.fuzzylite.term.Term;
import com.fuzzylite.term.Triangle;
import com.fuzzylite.variable.InputVariable;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import ressources.Util;

public class InputVariablesEditorPresenter implements Initializable {

	@FXML
	AreaChart<Double, Double> inPutVarChart;

	@FXML
	NumberAxis xAxis;

	@FXML
	NumberAxis yAxis;

	@FXML
	TextField termNameTextField;

	@FXML
	TextField varNameTextField;

	@FXML
	TextField maximumTextField;

	@FXML
	TextField minimumTextField;

	@FXML
	TextField inputValueTextField;

	@FXML
	ComboBox<String> termFormComboBox;

	@FXML
	Button doneButton;

	@FXML
	Button cancelButton;

	@FXML
	Button removeTermButton;

	@FXML
	HBox parameterHBox;

	@FXML
	TextField heightTextField;

	@FXML
	Button doneEditingTermButton;

	@Inject
	Engine engine;

	private InputVariable inputVariable;

	private boolean changed = false;

	private ObservableList<String> termFroms = FXCollections.observableArrayList("Triangle", "Trapezoid", "Ramp");

	private XYChart.Series activatedSeries;

	private Term activatedTerm;

	private boolean newInputVariable = false;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
	}

	/**
	 * Refrehes all GUI items
	 */
	public void refresh() {
		setUpAllItems();
		setUpAreaChart();
		drawAreaChart(inputVariable);
	}

	/**
	 * Initializes all ComboBoxes of the View
	 */
	private void setComboBoxes() {
		termFormComboBox.setItems(termFroms);
	}

	/**
	 * Sets up range and tick unit for xAxis and yAxis
	 */
	private void setUpAreaChart() {

		// set the maximum range for the yAxis
		yAxis.setAutoRanging(false);
		yAxis.setUpperBound(1.0);

		// set the tick unit for the yAxis
		yAxis.setTickUnit(0.25);

		// set the maximum range for the xAxis
		xAxis.setAutoRanging(false);
		xAxis.setUpperBound(inputVariable.getMaximum());
		xAxis.setLowerBound(inputVariable.getMinimum());

		// set the tick unit for the xAxis
		xAxis.setTickUnit(1.0);
	}

	public void setUpAllItems() {
		varNameTextField.setText(inputVariable.getName());
		if (inputVariable.getName().trim().isEmpty() || inputVariable.getName() == null) {
			doneButton.setDisable(true);
		}

		// Checks if name of inputVariable does not already exist, is null or
		// contains only spaces
		varNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue.trim().isEmpty() || newValue == null) {
				doneButton.setDisable(true);
			} else if (newValue.equals(inputVariable.getName())) {
				doneButton.setDisable(false);
			} else if (!newValue.equals(inputVariable.getName())) {
				if (!Util.checkIfVariableAlreadyExists(newValue, engine)) {
					doneButton.setDisable(false);
				} else {
					doneButton.setDisable(true);
				}
			} else {
				doneButton.setDisable(false);
			}
		});

		inputValueTextField.setText(Double.toString(inputVariable.getInputValue()));
		minimumTextField.setText(Double.toString(inputVariable.getMinimum()));
		maximumTextField.setText(Double.toString(inputVariable.getMaximum()));
		setComboBoxes();
	}

	/**
	 * Draw the AreaChart out of the Terms of the OutputVariable
	 *
	 * @param OutputVariable
	 *            var
	 */
	private void drawAreaChart(InputVariable var) {
		inPutVarChart.getData().clear();
		for (Term term : var.getTerms()) {
			ArrayList<Double> parameters = Util.extractTermParameter(term);

			// Create new Chart series
			XYChart.Series series = new XYChart.Series();

			// Add data to chart and draw it
			if (Util.getTermForm(term) == "Ramp") {
				series.getData().add(new XYChart.Data(parameters.get(0), 0.0));
				series.getData().add(new XYChart.Data(parameters.get(1), 1.0));
				series.getData().add(new XYChart.Data(inputVariable.getMaximum(), 1.0));
			} else {
				for (int i = 0; i < parameters.size(); i++) {
					if (i == 0) {
						series.getData().add(new XYChart.Data(parameters.get(i), 0.0));
					} else if (i == parameters.size() - 1) {
						series.getData().add(new XYChart.Data(parameters.get(i), 0.0));
					} else {
						series.getData().add(new XYChart.Data(parameters.get(i), 1.0));
					}
				}
			}

			series.setName(term.getName());
			inPutVarChart.getData().add(series);
		}

		for (XYChart.Series series : inPutVarChart.getData()) {

			// add MouseEventHandler to each series of the terms
			series.getNode().addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent event) {

					// delete TextFields of the term
					parameterHBox.getChildren().clear();
					HBox hbox = new HBox();
					hbox.setHgrow(hbox, Priority.ALWAYS);
					parameterHBox.getChildren().addAll(hbox);

					removeTermButton.setDisable(false);
					doneEditingTermButton.setVisible(true);
					termFormComboBox.setDisable(false);

					// set effects to null
					for (XYChart.Series series : inPutVarChart.getData()) {
						series.getNode().setEffect(null);
					}

					// set clicked series as the current activated series
					activatedSeries = series;

					// set shadow on clicked series
					series.getNode().setEffect(new DropShadow());

					activatedTerm = Util.findTermInEngineFromSeries(series, inputVariable);

					ArrayList<Double> parameters = Util.extractTermParameter(activatedTerm);

					// create and add a TextField for each parameter of the Term
					for (int i = 0; i < parameters.size(); i++) {
						TextField textField = new TextField();
						textField.setAlignment(Pos.CENTER);
						textField.setText(Double.toString(parameters.get(i)));
						textField.setPrefWidth(75);
						textField.setOnKeyPressed(new EventHandler<KeyEvent>() {

							@Override
							public void handle(KeyEvent event) {
								if (event.getCode().equals(KeyCode.ENTER)) {
									editTerm();
								}
							}
						});
						HBox h = new HBox();
						h.setHgrow(h, Priority.ALWAYS);
						parameterHBox.getChildren().addAll(textField, h);
					}

					heightTextField.setVisible(true);

					// set up GUI items after click on a series
					termNameTextField.setText(activatedTerm.getName());
					termFormComboBox.setValue(Util.getTermForm(activatedTerm));
					heightTextField.setText(Double.toString(activatedTerm.getHeight()));

					termFormComboBox.valueProperty().addListener(new ChangeListener<String>() {

						@Override
						public void changed(ObservableValue<? extends String> observable, String oldValue,
								String newValue) {

							if (oldValue != "") {
								switch (newValue) {
								case "Triangle":
									if (activatedTerm != null) {
										Term activatedTerm = Util.findTermInEngineFromSeries(activatedSeries,
												inputVariable);
										Term term = Util.transformTermToTriangle(inputVariable, activatedTerm,
												Util.getTermForm(activatedTerm));
										if (term != null) {
											inputVariable.removeTerm(activatedTerm);
											inputVariable.addTerm(term);
											refresh();
											deselectTerm();
										}
									}
									break;
								case "Trapezoid":
									if (activatedTerm != null) {
										Term activatedTerm = Util.findTermInEngineFromSeries(activatedSeries,
												inputVariable);
										Term term = Util.transformTermToTrapezoid(inputVariable, activatedTerm,
												Util.getTermForm(activatedTerm));
										if (term != null) {
											inputVariable.removeTerm(activatedTerm);
											inputVariable.addTerm(term);
											refresh();
											deselectTerm();
										}
									}
									break;
								case "Ramp":
									if (activatedTerm != null) {
										Term activatedTerm = Util.findTermInEngineFromSeries(activatedSeries,
												inputVariable);
										Term term = Util.transformTermToRamp(inputVariable, activatedTerm,
												Util.getTermForm(activatedTerm));
										if (term != null) {
											inputVariable.removeTerm(activatedTerm);
											inputVariable.addTerm(term);
											refresh();
											deselectTerm();
										}

									}
									break;
								}
							}

						}

					});

				}

			});
		}
	}

	public void setInputVariable(InputVariable inputVariable) {
		this.inputVariable = inputVariable;
		refresh();
	}

	public boolean getChanged() {
		return changed;
	}

	public InputVariable getInputVariable() {
		return inputVariable;
	}

	/**
	 * Reads the Value of the varNameTextField on Enter pressed and changes the
	 * name of the OutputVariable in the Engine
	 *
	 * @param ke
	 */
	public void varNameTextFieldChange(KeyEvent ke) {
		if (ke.getCode().equals(KeyCode.ENTER)) {
			inputVariable.setName(varNameTextField.getText());
			refresh();
		}
	}

	/**
	 * Reads the Value of the maximumTextField on Enter pressed and changes the
	 * maximum of the OutputVariable in the Engine
	 *
	 * @param ke
	 */
	public void maximumTextFieldChange(KeyEvent ke) {
		if (ke.getCode().equals(KeyCode.ENTER)) {
			inputVariable.setMaximum(Double.parseDouble(maximumTextField.getText()));
			xAxis.setUpperBound(inputVariable.getMaximum());
			refresh();
		}
	}

	/**
	 * Reads the Value of the minimumTextField on Enter pressed and changes the
	 * minimum of the OutputVariable in the Engine
	 *
	 * @param ke
	 */
	public void minimumTextFieldChange(KeyEvent ke) {
		if (ke.getCode().equals(KeyCode.ENTER)) {
			inputVariable.setMinimum(Double.parseDouble(minimumTextField.getText()));
			xAxis.setLowerBound(inputVariable.getMinimum());
			refresh();
		}
	}

	/**
	 * Reads the Value of the defaultTextField on Enter pressed and changes the
	 * default value of the OutputVariable in the Engine
	 *
	 * @param ke
	 */
	public void inputValueTextFieldChange(KeyEvent ke) {
		if (ke.getCode().equals(KeyCode.ENTER)) {
			inputVariable.setInputValue(Double.parseDouble(inputValueTextField.getText()));
			refresh();
		}
	}

	private void updateInputVariable() {
		inputVariable.setName(varNameTextField.getText());
		inputVariable.setMaximum(Double.parseDouble(maximumTextField.getText()));
		inputVariable.setMinimum(Double.parseDouble(minimumTextField.getText()));
	}

	public void handleDoneButtonAction(ActionEvent event) {

		updateInputVariable();
		editTerm();
		deselectTerm();
		Stage stage = (Stage) doneButton.getScene().getWindow();
		changed = true;

		// varNameTextField.setStyle("-fx-text-box-border: red ;
		// -fx-focus-color: red ; -fx-control-inner-background: #FFCDCD;");

		stage.close();

	}

	public void handleCancelButtonAction(ActionEvent event) {
		Stage stage = (Stage) cancelButton.getScene().getWindow();
		stage.close();
	}

	public void onAddTermButtonPressed(ActionEvent event) {
		inputVariable.addTerm(new Triangle("", 0.000, 1.000, 2.000));
		refresh();
	}

	/**
	 * Button Handler - Remove the activated Term out of the engine and re-draws
	 * the Chart
	 *
	 * @param event
	 */
	public void onRemoveTermButtonPressed(ActionEvent event) {
		Term activatedTerm = Util.findTermInEngineFromSeries(activatedSeries, inputVariable);
		inputVariable.removeTerm(activatedTerm);
		deselectTerm();
		refresh();
	}

	public void doneEditingTermNameTextField(ActionEvent event) {
		Term term = Util.findTermInEngineFromSeries(activatedSeries, inputVariable);
		term.setName(termNameTextField.getText());
		refresh();
	}

	private void deselectTerm() {
		activatedSeries = null;
		for (XYChart.Series series : inPutVarChart.getData()) {
			series.getNode().setEffect(null);
		}
		parameterHBox.getChildren().clear();
		heightTextField.setVisible(false);
		doneEditingTermButton.setVisible(false);
		termNameTextField.setText("");
		termFormComboBox.setDisable(true);
	}

	/**
	 * Function that is called when the parameters of a term a edited
	 */
	private void editTerm() {
		String parameters = "";

		int i = 1;
		while (i < parameterHBox.getChildren().size()) {
			TextField textField = (TextField) parameterHBox.getChildren().get(i);
			parameters += textField.getText() + " ";

			i = i + 2;
		}

		if (activatedTerm != null) {
			inputVariable.getTerm(activatedTerm.getName()).configure(parameters);
			inputVariable.getTerm(activatedTerm.getName()).setName(termNameTextField.getText());
		}

		refresh();
	}

	/**
	 * ButtonListener for DoneEditingButton
	 *
	 * @param event
	 */
	public void onDoneEditingTermButtonPressed(ActionEvent event) {
		editTerm();
		deselectTerm();
	}

	/**
	 * TextFieldListener for HeightTextField
	 *
	 * @param ke
	 */
	public void heightTextFieldChange(KeyEvent ke) {
		Term term = null;
		for (Term helpTerm : inputVariable.getTerms()) {
			if (activatedSeries != null) {
				if (helpTerm.getName() == activatedSeries.getName()) {
					term = helpTerm;
				}

				term.setHeight(Double.parseDouble(heightTextField.getText()));
				refresh();
			} else {
				return;
			}
		}
	}

	/**
	 * Returns value of attribute newInputVariable
	 *
	 * @return boolean newInputVariable
	 */
	public boolean isNewInputVariable() {
		return newInputVariable;
	}

	/**
	 * Sets value of attribute newInputVariable
	 *
	 * @param newInputVariable
	 */
	public void setNewInputVariable(boolean newInputVariable) {
		this.newInputVariable = newInputVariable;
	}

}
