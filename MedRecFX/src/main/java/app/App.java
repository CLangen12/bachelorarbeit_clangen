package app;

import com.airhacks.afterburner.injection.Injector;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import main.MainView;


public class App extends Application {

	@Override
	public void start(Stage stage) throws Exception {

		MainView appView = new MainView();
		Scene scene = new Scene(appView.getView());
		// stage.setTitle("MedRec - FuzzyLite - " + engine.getName());
		final String uri = getClass().getResource("app.css").toExternalForm();
		scene.getStylesheets().add(uri);
		stage.setScene(scene);
		//stage.setMaximized(true);
		stage.show();
	}

	@Override
	public void stop() throws Exception {
		Injector.forgetAll();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
