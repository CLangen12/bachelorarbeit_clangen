package ruleBlockEditor;

import java.net.URL;
import java.util.ResourceBundle;

import javax.inject.Inject;

import com.fuzzylite.Engine;
import com.fuzzylite.norm.s.AlgebraicSum;
import com.fuzzylite.norm.s.BoundedSum;
import com.fuzzylite.norm.s.DrasticSum;
import com.fuzzylite.norm.s.EinsteinSum;
import com.fuzzylite.norm.s.HamacherSum;
import com.fuzzylite.norm.s.Maximum;
import com.fuzzylite.norm.s.NilpotentMaximum;
import com.fuzzylite.norm.s.NormalizedSum;
import com.fuzzylite.norm.t.AlgebraicProduct;
import com.fuzzylite.norm.t.BoundedDifference;
import com.fuzzylite.norm.t.DrasticProduct;
import com.fuzzylite.norm.t.EinsteinProduct;
import com.fuzzylite.norm.t.HamacherProduct;
import com.fuzzylite.norm.t.Minimum;
import com.fuzzylite.norm.t.NilpotentMinimum;
import com.fuzzylite.rule.Rule;
import com.fuzzylite.rule.RuleBlock;
import com.fuzzylite.term.Term;
import com.fuzzylite.variable.InputVariable;
import com.fuzzylite.variable.OutputVariable;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import ressources.Util;

public class RuleBlockEditorPresenter implements Initializable {

	@FXML
	TextField ruleBlockNameTextField;

	@FXML
	ComboBox<String> conjunctionMethod;

	@FXML
	ComboBox<String> disjunctionMethod;

	@FXML
	ComboBox<String> activationMethod;

	@FXML
	ComboBox<String> inputVariablesComboBox;

	@FXML
	ComboBox<String> inputVariableTermComboBox;

	@FXML
	ComboBox<String> hedgeItemsComboBox;

	@FXML
	ComboBox<String> outputVariablesComboBox;

	@FXML
	ComboBox<String> outputVariableTermComboBox;

	@FXML
	ComboBox<String> secondHedgeItemsComboBox;

	@FXML
	ScrollPane rulesScrollPane;

	@FXML
	Button doneButton;

	@FXML
	Button cancelButton;

	@FXML
	Button ifButton;

	@FXML
	Button andButton;

	private VBox scrollPaneContent = new VBox();

	private RuleBlock ruleBlock;

	private boolean changed;

	private ObservableList<String> conjunctionMethods = FXCollections.observableArrayList("AlgebraicProduct",
			"BoundedDifference", "DrasticProduct", "EinsteinProduct", "HamacherProduct", "Minimum", "NilpotentMinimum");

	private ObservableList<String> disjunctionMethods = FXCollections.observableArrayList("AlgebraicSum", "BoundedSum",
			"DrasticSum", "EinsteinSum", "HamacherSum", "Maximum", "NilpotentMaximum", "NormalizedSum");

	private ObservableList<String> activationMethods = FXCollections.observableArrayList("AlgebraicProduct",
			"BoundedDifference", "DrasticProduct", "EinsteinProduct", "HamacherProduct", "Minimum", "NilpotentMinimum");

	private ObservableList<String> inputVariables = FXCollections.observableArrayList();

	private ObservableList<String> inputVariableTerms = FXCollections.observableArrayList();

	private ObservableList<String> hedgeItems = FXCollections.observableArrayList("", "any", "extremly", "not",
			"seldom", "somewhat", "very");

	private ObservableList<String> outputVariables = FXCollections.observableArrayList();

	private ObservableList<String> outputVariableTerms = FXCollections.observableArrayList();

	@Inject
	Engine engine;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		rulesScrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
		rulesScrollPane.setFitToWidth(true);

	}

	private void refresh() {
		ruleBlockNameTextField.setText(ruleBlock.getName());
	}

	public void createRuleTextFields() {

		for (int i = 0; i < ruleBlock.getRules().size(); i++) {

			TextField textField = new TextField();
			textField.setText(Util.extractRuleFromString(ruleBlock.getRule(i).toString()));

			scrollPaneContent.getChildren().add(textField);

		}

		TextField field = new TextField();
		field.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event) {
				ruleTextFieldEnterPressed(event);
			}
		});

		scrollPaneContent.getChildren().add(field);

		rulesScrollPane.setContent(scrollPaneContent);
	}

	private void setComboBoxes() {

		// Set conjunctionMethod-comboBox
		conjunctionMethod.setItems(conjunctionMethods);
		conjunctionMethod.setValue(Util.extractNameFormNorm(ruleBlock.getConjunction()));

		// Set disjunctionMethod-comboBox
		disjunctionMethod.setItems(disjunctionMethods);
		disjunctionMethod.setValue(Util.extractNameFormNorm(ruleBlock.getDisjunction()));

		// Set activationMethod-comboBox
		activationMethod.setItems(activationMethods);
		activationMethod.setValue(Util.extractNameFormNorm(ruleBlock.getActivation()));

		fillObservableArrayLists();
		inputVariablesComboBox.setItems(inputVariables);
		inputVariablesComboBox.setValue(inputVariables.get(0));

		outputVariablesComboBox.setItems(outputVariables);
		outputVariablesComboBox.setValue(outputVariables.get(0));

		hedgeItemsComboBox.setItems(hedgeItems);
		hedgeItemsComboBox.setValue(hedgeItems.get(0));

		secondHedgeItemsComboBox.setItems(hedgeItems);
		secondHedgeItemsComboBox.setValue(hedgeItems.get(0));
	}

	/**
	 * Takes the value of the conjuctionMethodComboBox and sets the conjunction
	 * method of the current engine
	 *
	 * @param event
	 */
	public void conjunctionMethodComboChanged(ActionEvent event) {

		String selection = conjunctionMethod.getValue();

		switch (selection) {
		case "AlgebraicProduct":
			ruleBlock.setConjunction(new AlgebraicProduct());
			break;
		case "BoundedDifference":
			ruleBlock.setConjunction(new BoundedDifference());
			break;
		case "DrasticProduct":
			ruleBlock.setConjunction(new DrasticProduct());
			break;
		case "EinsteinProduct":
			ruleBlock.setConjunction(new EinsteinProduct());
			break;
		case "HamacherProduct":
			ruleBlock.setConjunction(new HamacherProduct());
			break;
		case "Minimum":
			ruleBlock.setConjunction(new Minimum());
			break;
		case "NilpotentMinimum":
			ruleBlock.setConjunction(new NilpotentMinimum());
			break;
		}
	}

	/**
	 * Takes the value of the disjuctionMethodComboBox and sets the disjunction
	 * method of the current engine
	 *
	 * @param event
	 */
	public void disjunctionMethodComboChanged(ActionEvent event) {

		String selection = disjunctionMethod.getValue();

		switch (selection) {
		case "AlgebraicSum":
			ruleBlock.setDisjunction(new AlgebraicSum());
			break;
		case "BoundedSum":
			ruleBlock.setDisjunction(new BoundedSum());
			break;
		case "DrasticSum":
			ruleBlock.setDisjunction(new DrasticSum());
			break;
		case "EinsteinSum":
			ruleBlock.setDisjunction(new EinsteinSum());
			break;
		case "HamacherSum":
			ruleBlock.setDisjunction(new HamacherSum());
			break;
		case "Maximum":
			ruleBlock.setDisjunction(new Maximum());
			break;
		case "NilpotentSum":
			ruleBlock.setDisjunction(new NilpotentMaximum());
			break;
		case "NormalizedSum":
			ruleBlock.setDisjunction(new NormalizedSum());
			break;
		}
	}

	/**
	 * Takes the value of the activationMethodComboBox and sets the activation
	 * method of the current engine
	 *
	 * @param event
	 */
	public void activationMethodComboChanged(ActionEvent event) {

		String selection = activationMethod.getValue();

		switch (selection) {
		case "AlgebraicProduct":
			ruleBlock.setActivation(new AlgebraicProduct());
			break;
		case "BoundedDifference":
			ruleBlock.setActivation(new BoundedDifference());
			break;
		case "DrasticProduct":
			ruleBlock.setActivation(new DrasticProduct());
			break;
		case "EinsteinProduct":
			ruleBlock.setActivation(new EinsteinProduct());
			break;
		case "HamacherProduct":
			ruleBlock.setActivation(new HamacherProduct());
			break;
		case "Minimum":
			ruleBlock.setActivation(new Minimum());
			break;
		case "NilpotentMinimum":
			ruleBlock.setActivation(new NilpotentMinimum());
			break;
		}
	}

	public void inputVariableComboChanged(ActionEvent event) {

	}

	/**
	 * Set the RuleBlock and refreshes all UI Elements
	 *
	 * @param ruleBlock
	 */
	public void setRuleBlock(RuleBlock ruleBlock) {
		this.ruleBlock = ruleBlock;
		setComboBoxes();
		refresh();
		createRuleTextFields();

	}

	/**
	 * Gets the RuleBlock
	 *
	 * @return RuleBlock ruleBlock
	 */
	public RuleBlock getRuleBlock() {
		return ruleBlock;
	}

	/**
	 * Gets Boolean changed
	 *
	 * @return Boolean Changed
	 */
	public boolean isChanged() {
		return changed;
	}

	/**
	 * Sets Boolean changed
	 *
	 * @param changed
	 */
	public void setChanged(boolean changed) {
		this.changed = changed;
	}

	/**
	 * Saves the rule of each TextField as one Rule in the RuleBlock
	 */
	public void saveRulesInRuleBlock() {

		ruleBlock.getRules().clear();

		for (Node node : scrollPaneContent.getChildren()) {
			TextField field = (TextField) node;
			if (!field.getText().isEmpty()) {
				ruleBlock.addRule(Rule.parse(field.getText().toString(), engine));
			}
		}
	}

	/**
	 * Is called when the doneButton is pressed
	 *
	 * @param event
	 */
	public void handleDoneButtonAction(ActionEvent event) {
		saveRulesInRuleBlock();
		setChanged(true);
		Stage stage = (Stage) doneButton.getScene().getWindow();
		stage.close();
	}

	/**
	 * Is called when the cancelButton is pressed
	 *
	 * @param event
	 */
	public void handleCancelButtonAction(ActionEvent event) {
		Stage stage = (Stage) cancelButton.getScene().getWindow();
		stage.close();
	}

	/**
	 * Is called when the ifButton is pressed
	 *
	 * @param event
	 */
	public void onIfButtonPressed(ActionEvent event) {
		TextField textField = getCurrentlyFocusedTextField();

		if (textField != null) {
			String text = textField.getText().toString();
			text += "if";
			textField.setText(text);
		}
	}

	/**
	 * Is called when the andButton is pressed
	 *
	 * @param event
	 */
	public void onAndButtonPressed(ActionEvent event) {
		TextField textField = getCurrentlyFocusedTextField();

		if (textField != null) {
			String text = textField.getText().toString();
			text += " and";
			textField.setText(text);
		}
	}

	/**
	 * Is called when the thenButton is pressed
	 *
	 * @param event
	 */
	public void onThenButtonPressed(ActionEvent event) {
		TextField textField = getCurrentlyFocusedTextField();

		if (textField != null) {
			String text = textField.getText().toString();
			text += " then";
			textField.setText(text);
		}
	}

	/**
	 * Is called when the orButton is pressed
	 *
	 * @param event
	 */
	public void onOrButtonPressed(ActionEvent event) {
		TextField textField = getCurrentlyFocusedTextField();

		if (textField != null) {
			String text = textField.getText().toString();
			text += " or";
			textField.setText(text);
		}
	}

	/**
	 * Is called when the isButton is pressed
	 *
	 * @param event
	 */
	public void onIsButtonPressed(ActionEvent event) {
		TextField textField = getCurrentlyFocusedTextField();

		if (textField != null) {
			String text = textField.getText().toString();
			text += " is";
			textField.setText(text);
		}
	}

	/**
	 * Returns the currently focused TextField
	 *
	 * @return currently focused TextField
	 */
	private TextField getCurrentlyFocusedTextField() {
		Scene scene = doneButton.getScene();
		Node node = scene.getFocusOwner();
		TextField field = (TextField) node;
		return field;
	}

	/**
	 *
	 * @param ke
	 */
	public void ruleTextFieldEnterPressed(KeyEvent ke) {
		if (ke.getCode().equals(KeyCode.ENTER)) {
			TextField helpField = getCurrentlyFocusedTextField();

			TextField field = new TextField();
			field.setOnKeyPressed(new EventHandler<KeyEvent>() {
				@Override
				public void handle(KeyEvent event) {
					ruleTextFieldEnterPressed(event);
				}
			});

			scrollPaneContent.getChildren().add(field);
			field.requestFocus();

			helpField.getOnKeyPressed().handle(null);

			refresh();
		}
	}

	/**
	 * Fills ObservableArrayLists with all InputVariables and OutputVariables of
	 * the FunctionBlock
	 */
	private void fillObservableArrayLists() {
		for (InputVariable input : engine.getInputVariables()) {
			inputVariables.add(input.getName());
		}
		for (OutputVariable output : engine.getOutputVariables()) {
			outputVariables.add(output.getName());
		}
	}

	/**
	 * Fills inputVariableTermComboBox with the terms of the selected
	 * InputVariable
	 *
	 * @param event
	 */
	public void inputVariablesComboChanged(ActionEvent event) {
		String selection = inputVariablesComboBox.getValue();

		inputVariableTerms.clear();

		for (Term t : engine.getInputVariable(selection).getTerms()) {
			inputVariableTerms.add(t.getName());
		}

		inputVariableTermComboBox.setItems(inputVariableTerms);
		inputVariableTermComboBox.setValue(inputVariableTerms.get(0));

		/*
		 * TextField field = getCurrentlyFocusedTextField();
		 *
		 * if (selection != null) { String text = field.getText().toString();
		 * text += selection; field.setText(text); }
		 */

	}

	public void outputVariablesComboChanged(ActionEvent event) {
		String selection = outputVariablesComboBox.getValue();

		outputVariableTerms.clear();

		for (Term t : engine.getOutputVariable(selection).getTerms()) {
			outputVariableTerms.add(t.getName());
		}

		outputVariableTermComboBox.setItems(outputVariableTerms);
		outputVariableTermComboBox.setValue(outputVariableTerms.get(0));
	}

}
