package outputVariable;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.inject.Inject;
import javax.swing.RootPaneContainer;

import com.fuzzylite.Engine;
import com.fuzzylite.defuzzifier.Bisector;
import com.fuzzylite.defuzzifier.Centroid;
import com.fuzzylite.defuzzifier.LargestOfMaximum;
import com.fuzzylite.defuzzifier.MeanOfMaximum;
import com.fuzzylite.defuzzifier.SmallestOfMaximum;
import com.fuzzylite.defuzzifier.WeightedAverage;
import com.fuzzylite.defuzzifier.WeightedSum;
import com.fuzzylite.norm.t.AlgebraicProduct;
import com.fuzzylite.norm.t.BoundedDifference;
import com.fuzzylite.norm.t.DrasticProduct;
import com.fuzzylite.norm.t.EinsteinProduct;
import com.fuzzylite.norm.t.HamacherProduct;
import com.fuzzylite.norm.t.Minimum;
import com.fuzzylite.norm.t.NilpotentMinimum;
import com.fuzzylite.term.Term;
import com.fuzzylite.variable.OutputVariable;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import outputVariablesEditor.OutputVariablesEditorPresenter;
import outputVariablesEditor.OutputVariablesEditorView;
import ressources.Util;
import ruleEditor.RuleEditorView;

public class OutputVariablePresenter implements Initializable {

	@FXML
	AreaChart<Double, Double> outPutVarChart;

	@FXML
	Label outPutVar;

	@FXML
	ComboBox<String> accumulationMethod;

	@FXML
	ComboBox<String> defuzzifyMethod;

	@FXML
	Label varActivationLabel;

	@FXML
	NumberAxis xAxis;

	@FXML
	NumberAxis yAxis;

	@Inject
	Engine engine;

	@FXML
	TextField defaultTextField;

	@FXML
	TextField minimumTextField;

	@FXML
	TextField maximumTextField;

	@FXML
	Button editButton;

	// List with all possible defuuzification methods
	private ObservableList<String> defuzzifyMethods = FXCollections.observableArrayList("Bisector", "Centroid",
			"LargestOfMaximum", "MeanOfMaximum", "SmallestOfMaximum", "WeightedAverage", "WeightedSum");

	// List with all possible accumulation methods
	private ObservableList<String> accumulationMethods = FXCollections.observableArrayList("AlgebraicSum", "BoundedSum",
			"DrasticSum", "EinsteinSum", "HamacherSum", "Maximum", "NilpotentMaximum", "NormalizedSum");

	private OutputVariable tmpOutput;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}

	/**
	 * Initializes all ComboBoxes of the View
	 */
	private void setComboBoxes() {
		defuzzifyMethod.setItems(defuzzifyMethods);
		defuzzifyMethod.setValue(Util.extractDefuzzificationMethod(engine.getOutputVariable(0).getDefuzzifier()));
		accumulationMethod.setItems(accumulationMethods);
		accumulationMethod
				.setValue(Util.extractNameFormNorm(engine.getOutputVariable(0).fuzzyOutput().getAccumulation()));
	}

	/**
	 * Initializes all TextFields of the View
	 */
	private void setTextFields() {
		minimumTextField.setText(Double.toString(engine.getOutputVariable(0).getMinimum()));
		defaultTextField.setText(Double.toString(engine.getOutputVariable(0).getDefaultValue()));
		maximumTextField.setText(Double.toString(engine.getOutputVariable(0).getMaximum()));
	}

	/**
	 * Draw the AreaChart out of the Terms of the OutputVariable
	 *
	 * @param OutputVariable
	 *            var
	 */
	private void drawAreaChart(OutputVariable var) {
		outPutVarChart.getData().clear();
		for (Term term : var.getTerms()) {
			ArrayList<Double> parameters = Util.extractTermParameter(term);

			XYChart.Series series = new XYChart.Series();

			if(Util.getTermForm(term)=="Ramp"){
				series.getData().add(new XYChart.Data(parameters.get(0), 0.0));
				series.getData().add(new XYChart.Data(parameters.get(1), 1.0));
				series.getData().add(new XYChart.Data(engine.getOutputVariable(0).getMaximum(), 1.0));
			} else {
				for (int i = 0; i < parameters.size(); i++) {
					if (i == 0) {
						series.getData().add(new XYChart.Data(parameters.get(i), 0.0));
					} else if (i == parameters.size() - 1) {
						series.getData().add(new XYChart.Data(parameters.get(i), 0.0));
					} else {
						series.getData().add(new XYChart.Data(parameters.get(i), 1.0));
					}
				}
			}



			series.setName(term.getName());
			outPutVarChart.getData().add(series);
		}
	}

	/**
	 * Sets up range and tick unit for xAxis and yAxis
	 */
	private void setUpAreaChart() {

		// set the maximum range for the yAxis
		yAxis.setAutoRanging(false);
		yAxis.setUpperBound(1.0);

		// set the tick unit for the yAxis
		yAxis.setTickUnit(0.25);

		// set the maximum range for the xAxis
		xAxis.setAutoRanging(false);
		xAxis.setUpperBound(engine.getOutputVariable(0).getMaximum());
		xAxis.setLowerBound(engine.getOutputVariable(0).getMinimum());

		// set the tick unit for the xAxis
		xAxis.setTickUnit(1.0);
	}

	/**
	 * Refresh all UI elements of the OutputVariable
	 */
	public void refresh() {
		outPutVar.setText(engine.getOutputVariable(0).getName());
		varActivationLabel.setText(engine.getOutputVariable(0).fuzzyOutputValue());
		setTextFields();
		setComboBoxes();
		setUpAreaChart();
		drawAreaChart(engine.getOutputVariable(0));
	}

	/**
	 * Open the new View when the EditButton is clicked
	 *
	 * @param event
	 * @throws IOException
	 */
	public void onEditButtonPressed(ActionEvent event) throws IOException {

		javafx.application.Platform.runLater(new Runnable() {

			@Override
			public void run() {
				Stage stage = new Stage();
				OutputVariablesEditorView view = new OutputVariablesEditorView();
				OutputVariablesEditorPresenter presenter = (OutputVariablesEditorPresenter) view.getPresenter();
				try {
					tmpOutput = engine.getOutputVariable(0).clone();
				} catch (CloneNotSupportedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				presenter.setOutputVariable(tmpOutput);
				Scene scene = new Scene(view.getView());
				stage.setScene(scene);
				stage.setTitle("Output Variable Editor");
				scene.getStylesheets().add("OutputVariablesEditor.css");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.show();
				stage.setOnHidden(new EventHandler<WindowEvent>() {

					@Override
					public void handle(WindowEvent event) {
						tmpOutput = new OutputVariable();
						if (presenter.getChanged()) {
							OutputVariable helpVar = presenter.getOutputVariable();
							updateOutputVariableInEngine(helpVar);
							refresh();
						}
					}
				});

			}
		});
	}

	/**
	 * Updates the OutputVariable in the engine
	 * @param helpVar
	 */
	private void updateOutputVariableInEngine(OutputVariable helpVar){
		engine.getOutputVariable(0).setTerms(helpVar.getTerms());
		engine.getOutputVariable(0).setName(helpVar.getName());
		engine.getOutputVariable(0).setMinimum(helpVar.getMinimum());
		engine.getOutputVariable(0).setMaximum(helpVar.getMaximum());
		engine.getOutputVariable(0).setDefaultValue(helpVar.getDefaultValue());
	}

	/**
	 * Reads the Value of the maximumTextField on Enter pressed and changes the
	 * maximum of the OutputVariable in the Engine
	 *
	 * @param ke
	 */
	public void maximumTextFieldChange(KeyEvent ke) {
		if (ke.getCode().equals(KeyCode.ENTER)) {
			engine.getOutputVariable(0).setMaximum(Double.parseDouble(maximumTextField.getText()));
			xAxis.setUpperBound(engine.getOutputVariable(0).getMaximum());
			refresh();
		}
	}

	/**
	 * Reads the Value of the minimumTextField on Enter pressed and changes the
	 * minimum of the OutputVariable in the Engine
	 *
	 * @param ke
	 */
	public void minimumTextFieldChange(KeyEvent ke) {
		if (ke.getCode().equals(KeyCode.ENTER)) {
			engine.getOutputVariable(0).setMinimum(Double.parseDouble(minimumTextField.getText()));
			xAxis.setLowerBound(engine.getInputVariable(0).getMinimum());
			refresh();
		}
	}

	/**
	 * Reads the Value of the defaultTextField on Enter pressed and changes the
	 * default value of the OutputVariable in the Engine
	 *
	 * @param ke
	 */
	public void defaultTextFieldChange(KeyEvent ke) {
		if (ke.getCode().equals(KeyCode.ENTER)) {
			engine.getOutputVariable(0).setDefaultValue(Double.parseDouble(defaultTextField.getText()));
			refresh();
		}
	}

	/**
	 * Takes the value of the defuzzifyMethod ComboBox and sets the
	 * defuzzification method method of the current engine
	 *
	 * @param event
	 */
	public void defuzzifyMethodComboChanged(ActionEvent event) {

		String selection = defuzzifyMethod.getValue();

		switch (selection) {
		case "Bisector":
			engine.getOutputVariable(0).setDefuzzifier(new Bisector());
			break;
		case "Centroid":
			engine.getOutputVariable(0).setDefuzzifier(new Centroid());
			break;
		case "LargestOfMaximum":
			engine.getOutputVariable(0).setDefuzzifier(new LargestOfMaximum());
			break;
		case "MeanOfMaximum":
			engine.getOutputVariable(0).setDefuzzifier(new MeanOfMaximum());
			break;
		case "SmallestOfMaximum":
			engine.getOutputVariable(0).setDefuzzifier(new SmallestOfMaximum());
			break;
		case "WeightedAverage":
			engine.getOutputVariable(0).setDefuzzifier(new WeightedAverage());
			break;
		case "NilpotentMinimum":
			engine.getOutputVariable(0).setDefuzzifier(new WeightedSum());
			break;
		}
	}

}
