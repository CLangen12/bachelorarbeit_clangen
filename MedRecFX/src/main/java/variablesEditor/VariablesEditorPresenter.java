package variablesEditor;

import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;

import javax.inject.Inject;

import com.fuzzylite.Engine;
import com.fuzzylite.term.Triangle;
import com.fuzzylite.variable.InputVariable;

import inputVariable.InputVariablePresenter;
import inputVariable.InputVariableView;
import inputVariablesEditor.InputVariablesEditorPresenter;
import inputVariablesEditor.InputVariablesEditorView;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import outputVariable.OutputVariablePresenter;
import outputVariable.OutputVariableView;

public class VariablesEditorPresenter implements Initializable {

	@FXML
	AnchorPane left;

	@FXML
	AnchorPane right;

	@FXML
	ScrollPane variablesScollPane;

	@Inject
	Engine engine;

	private Map<InputVariableView, InputVariablePresenter> inputVariables;

	private OutputVariableView outputVariable;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		variablesScollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
		variablesScollPane.setFitToWidth(true);

		outputVariable = new OutputVariableView();

		right.getChildren().add(outputVariable.getView());

		right.setTopAnchor(outputVariable.getView(), 0.0);
		right.setBottomAnchor(outputVariable.getView(), 0.0);
		right.setLeftAnchor(outputVariable.getView(), 0.0);
		right.setRightAnchor(outputVariable.getView(), 0.0);

		// left.setTopAnchor(variablesScollPane, 0.0);
		left.setBottomAnchor(variablesScollPane, 0.0);
		left.setLeftAnchor(variablesScollPane, 0.0);
		left.setRightAnchor(variablesScollPane, 0.0);

	}

	public void createInputVariableViews() {
		VBox content = new VBox();
		for (int i = 0; i < engine.getInputVariables().size(); i++) {
			InputVariableView inputVariable = new InputVariableView();
			InputVariablePresenter inputVariablePresenter = (InputVariablePresenter) inputVariable.getPresenter();
			inputVariablePresenter.setInputVariable(engine.getInputVariable(i));
			// inputVariables.put(inputVariable, inputVariablePresenter);
			inputVariable.getView().autosize();
			content.getChildren().add(inputVariable.getView());

		}
		variablesScollPane.setContent(content);
	}

	/**
	 * Sets the OutputVariable and adds all InputVariables to the VBox
	 *
	 * @param content
	 */
	public void setUpVariablesEditor() {
		OutputVariablePresenter outputVariablePresenter = (OutputVariablePresenter) outputVariable.getPresenter();
		outputVariablePresenter.refresh();

		createInputVariableViews();
	}

	public void onAddButtonClicked(ActionEvent event) throws IOException {
		javafx.application.Platform.runLater(new Runnable() {

			@Override
			public void run() {
				Stage stage = new Stage();
				InputVariablesEditorView view = new InputVariablesEditorView();
				InputVariablesEditorPresenter presenter = (InputVariablesEditorPresenter) view.getPresenter();

				InputVariable tmpInput = new InputVariable();
				tmpInput.setName("Neues Symptom");
				tmpInput.addTerm(new Triangle("nicht_vorhanden", 0.000, 2.500, 5.000));
				tmpInput.addTerm(new Triangle("vorhanden", 5.000, 7.500, 10.000));
				tmpInput.setRange(0.000, 10.000);

				presenter.setInputVariable(tmpInput);
				presenter.setNewInputVariable(true);
				Scene scene = new Scene(view.getView());
				stage.setScene(scene);
				stage.setTitle("Input Variable Editor");
				// scene.getStylesheets().add("OutputVariablesEditor.css");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.show();

				stage.setOnHidden(new EventHandler<WindowEvent>() {

					@Override
					public void handle(WindowEvent event) {

						if (presenter.getChanged()) {
							engine.addInputVariable(presenter.getInputVariable());
							setUpVariablesEditor();
						}
					}
				});

			}
		});
	}

}
