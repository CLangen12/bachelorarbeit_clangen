package ressources;

import java.util.ArrayList;

import com.fuzzylite.Engine;
import com.fuzzylite.defuzzifier.Defuzzifier;
import com.fuzzylite.norm.Norm;
import com.fuzzylite.term.Ramp;
import com.fuzzylite.term.Term;
import com.fuzzylite.term.Trapezoid;
import com.fuzzylite.term.Triangle;
import com.fuzzylite.variable.InputVariable;
import com.fuzzylite.variable.OutputVariable;
import com.fuzzylite.variable.Variable;

import javafx.scene.chart.XYChart;

public class Util {

	/**
	 * Extracts the name of the given Norm
	 *
	 * @param norm
	 * @return String Norm
	 */
	public static String extractNameFormNorm(Norm norm) {
		String help = "";
		if (norm != null) {
			int index = norm.toString().indexOf('@');
			help = (String) norm.toString().subSequence(21, index);
			return help;
		} else {
			return null;
		}
	}

	/**
	 * Extracts the name of the defuzzification method from the class
	 * defuzzifier
	 *
	 * @param defuzzifier
	 * @return String defuzzification method
	 */
	public static String extractDefuzzificationMethod(Defuzzifier defuzzifier) {
		String help = "";
		if (defuzzifier != null) {
			int index = defuzzifier.toString().indexOf('@');
			help = (String) defuzzifier.toString().subSequence(26, index);
			return help;
		} else {
			return null;
		}
	}

	/**
	 * Creates an ArrayList of double values form the parameter attribute
	 * (String) of the class Term
	 *
	 * @param term
	 * @return ArrayList<Double> parameter
	 */
	public static ArrayList<Double> extractTermParameter(Term term) {
		ArrayList<Double> parameter = new ArrayList<Double>();
		String parameterString = term.parameters();
		String help;

		parameterString = parameterString.replace(",", ".");

		while (parameterString.length() > 9) {
			help = parameterString.substring(0, parameterString.indexOf(" "));
			parameterString = parameterString.substring(parameterString.indexOf(" ") + 1);

			parameter.add(Double.parseDouble(help));
		}

		parameter.add(Double.parseDouble(parameterString));
		return parameter;
	}

	/**
	 * Extracts the Form of the Term from string
	 *
	 * @param term
	 * @return String form
	 */
	public static String getTermForm(Term term) {
		if (term.toString().contains("Triangle"))
			return "Triangle";
		else if (term.toString().contains("Trapezoid"))
			return "Trapezoid";
		else if (term.toString().contains("Ramp"))
			return "Ramp";
		return "";
	}

	/**
	 * Find matching term for activated Chart Series
	 *
	 * @param series
	 * @param engine
	 * @return Term activatedTerm
	 */
	public static Term findTermInEngineFromSeries(XYChart.Series series, Variable variable) {

		if (series != null && variable != null) {
			Term term = null;

			for (Term helpTerm : variable.getTerms()) {
				if (helpTerm.getName() == series.getName()) {
					term = helpTerm;
				}
			}
			return term;
		} else
			return null;
	}

	/**
	 * Transforms the given Term to a Triangle
	 *
	 * @param variable
	 * @param term
	 * @param currentTermForm
	 * @return Term term
	 */
	public static Term transformTermToTriangle(Variable variable, Term term, String currentTermForm) {

		ArrayList<Double> parameter = extractTermParameter(term);
		String parameterString = "";

		if (currentTermForm == "Trapezoid") {
			parameterString += fillUpStringForParameterLength(Double.toString(parameter.get(0))) + " ";

			double sum = 0;

			for (int i = 1; i < parameter.size() - 1; i++) {
				sum += parameter.get(i);
			}

			parameterString += fillUpStringForParameterLength(Double.toString(sum / (parameter.size() - 2))) + " ";
			parameterString += fillUpStringForParameterLength(Double.toString(parameter.get(parameter.size() - 1)));

			Term newTerm = new Triangle();
			newTerm.setName(term.getName());
			newTerm.configure(parameterString);

			return newTerm;
		} else if (currentTermForm == "Ramp") {
			parameterString += fillUpStringForParameterLength(Double.toString(parameter.get(0))) + " ";
			parameterString += fillUpStringForParameterLength(Double.toString(parameter.get(1))) + " ";
			parameterString += fillUpStringForParameterLength(Double.toString(variable.getMaximum()));

			Term newTerm = new Triangle();
			newTerm.setName(term.getName());
			newTerm.configure(parameterString);

			return newTerm;
		}
		return null;
	}

	/**
	 * Transforms the given Term to a Trapezoid
	 *
	 * @param variable
	 * @param term
	 * @param currentTermForm
	 * @return Term term
	 */
	public static Term transformTermToTrapezoid(Variable variable, Term term, String currentTermForm) {

		ArrayList<Double> parameter = extractTermParameter(term);
		String parameterString = "";

		if (currentTermForm == "Triangle" || currentTermForm == "Ramp") {
			parameterString += fillUpStringForParameterLength(Double.toString(parameter.get(0))) + " ";
			parameterString += fillUpStringForParameterLength(
					Double.toString((parameter.get(parameter.size() - 1) - parameter.get(0)) * 0.25 + parameter.get(0))
							+ " ");
			parameterString += fillUpStringForParameterLength(
					Double.toString((parameter.get(parameter.size() - 1) - parameter.get(0)) * 0.75 + parameter.get(0))
							+ " ");
			parameterString += fillUpStringForParameterLength(Double.toString(parameter.get(parameter.size() - 1)));

			Term newTerm = new Trapezoid();
			newTerm.setName(term.getName());
			newTerm.configure(parameterString);

			return newTerm;
		}
		return null;
	}

	/**
	 * Transforms the given Term to a Ramp
	 *
	 * @param variable
	 * @param term
	 * @param currentTermForm
	 * @return Term term
	 */
	public static Term transformTermToRamp(Variable variable, Term term, String currentTermForm) {

		ArrayList<Double> parameter = extractTermParameter(term);
		String parameterString = "";

		if (currentTermForm == "Trapezoid") {
			parameterString += fillUpStringForParameterLength(Double.toString(parameter.get(0))) + " ";
			parameterString += fillUpStringForParameterLength(Double.toString(parameter.get(1)));

			Term newTerm = new Ramp();
			newTerm.setName(term.getName());
			newTerm.configure(parameterString);

			return newTerm;
		} else if (currentTermForm == "Triangle") {
			parameterString += fillUpStringForParameterLength(Double.toString(parameter.get(0))) + " ";
			parameterString += fillUpStringForParameterLength(Double.toString(parameter.get(1)));

			Term newTerm = new Ramp();
			newTerm.setName(term.getName());
			newTerm.configure(parameterString);

			return newTerm;
		}

		return null;
	}

	/**
	 * FuzzyLite needs parameters as Strings in the format 2.000 this method
	 * creates them
	 *
	 * @param s
	 * @return String with length 6
	 */
	public static String fillUpStringForParameterLength(String s) {
		String help = s;
		for (int i = help.length(); i < 5; i++) {
			help += "0";
		}
		return help;
	}

	/**
	 * Extracts a Rule from a String
	 *
	 * @param ruleString
	 * @return Rule rule
	 */
	public static String extractRuleFromString(String ruleString) {
		String rule = ruleString.substring(ruleString.indexOf("if"));
		return rule;
	}

	/**
	 * Checks if the InputVariable with the given name already exists in the
	 * engine
	 *
	 * @param inputVariableName
	 * @return Boolean
	 */
	public static boolean checkIfVariableAlreadyExists(String variableName, Engine engine) {
		for (InputVariable var : engine.getInputVariables()) {
			if (var.getName().equals(variableName)) {
				return true;
			}
		}
		for (OutputVariable outputVar : engine.getOutputVariables()) {
			if (outputVar.getName().equals(variableName)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Searches the index of the given Inputvariable in the Engine
	 * 
	 * @param inputVariable
	 * @param engine
	 * @return int Index
	 */
	public static int getIndexOfInputVariableInEngine(InputVariable inputVariable, Engine engine) {

		for (int i = 0; i < engine.getInputVariables().size(); i++) {
			if (engine.getInputVariable(i).equals(inputVariable)) {
				return i;
			}
		}
		return 0;
	}

}
