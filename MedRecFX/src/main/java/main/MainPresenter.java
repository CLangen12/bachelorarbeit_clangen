package main;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.inject.Inject;

import com.airhacks.afterburner.injection.Injector;
import com.fuzzylite.Engine;
import com.fuzzylite.imex.FllImporter;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.ToolBar;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import ruleEditor.RuleEditorPresenter;
import ruleEditor.RuleEditorView;
import variablesEditor.VariablesEditorPresenter;
import variablesEditor.VariablesEditorView;

public class MainPresenter implements Initializable {

	@FXML
	AnchorPane top;

	@FXML
	AnchorPane bottom;

	@FXML
	MenuBar mainMenuBar;

	@FXML
	AnchorPane mainAnchorPane;

	@FXML
	ToolBar mainToolBar;

	@FXML
	Label noFunctionBlockLoadedLabel;

	@FXML
	AnchorPane noFunctionBlockLoadedPane;

	@Inject
	Engine engine;

	@FXML
	Button loadFLLButton;

	@FXML
	Button loadFunctionBlockButton;

	private RuleEditorView ruleEditor;

	private VariablesEditorView variablesEditor;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		/*
		 * final String os = System.getProperty ("os.name"); if (os != null &&
		 * os.startsWith("Mac")){
		 * mainMenuBar.useSystemMenuBarProperty().set(true); }
		 */

	}

	/**
	 * ActionListen for loadFLLButton
	 *
	 * @param event
	 */
	public void LoadFLLAction(ActionEvent event) {
		loadFLL();
	}

	/**
	 * ActionListen for loadFunctionBlockButton
	 *
	 * @param event
	 */
	public void LoadFunctionBlockAction(ActionEvent event) {
		loadFunctionBlock();
	}

	/*
	 * public void exportFLLAction(ActionEvent event) {
	 *
	 * FllExporter exporter = new FllExporter(); FileChooser fc = new
	 * FileChooser(); try { // exporter.toFile(new
	 * File("//Users//Christoph//Desktop//" + // engine.getName() + ".txt"),
	 * engine); // fc.getExtensionFilters().addAll(new ExtensionFilter(
	 * "FLL Files", // "*.txt")); // fc.setInitialFileName(engine.getName()); //
	 * exporter.toFile(fc.showSaveDialog(null), engine); } catch (IOException e)
	 * { e.printStackTrace(); } }
	 */

	/**
	 * KeyListener for the Enter-key a loadFLLButton
	 *
	 * @param ke
	 */
	public void SubmitLoadFLL(KeyEvent ke) {
		if (ke.getCode().equals(KeyCode.ENTER)) {
			loadFLL();
		}
	}

	/**
	 * KeyListener for the Enter-key a loadFLLButton
	 *
	 * @param ke
	 */
	public void SubmitLoadFunctionBlock(KeyEvent ke) {
		if (ke.getCode().equals(KeyCode.ENTER)) {
			System.out.println("Enter on loadFunctionBlockButton pressed!");
		}
	}

	/**
	 * Select a File from FileChooser and load the FLL
	 */
	public void loadFLL() {
		Injector.forgetAll();

		FileChooser fc = new FileChooser();
		fc.getExtensionFilters().addAll(new ExtensionFilter("FLL Files", "*.fll"));
		File selectedFile = fc.showOpenDialog(null);

		if (selectedFile != null) {
			FllImporter importer = new FllImporter();
			try {
				Engine engine = importer.fromFile(selectedFile);

				Map<Object, Object> customProperties = new HashMap<>();
				customProperties.put("engine", engine);
				Injector.setConfigurationSource(customProperties::get);

				ruleEditor = new RuleEditorView();
				RuleEditorPresenter ruleEditorPresenter = (RuleEditorPresenter) ruleEditor.getPresenter();
				bottom.getChildren().add(ruleEditor.getView());
				ruleEditorPresenter.refresh();

				bottom.setTopAnchor(ruleEditor.getView(), 0.0);
				bottom.setBottomAnchor(ruleEditor.getView(), 0.0);
				bottom.setLeftAnchor(ruleEditor.getView(), 0.0);
				bottom.setRightAnchor(ruleEditor.getView(), 0.0);

				VariablesEditorView variablesEditor = new VariablesEditorView();
				VariablesEditorPresenter variablesEditorPresenter = (VariablesEditorPresenter) variablesEditor
						.getPresenter();
				variablesEditorPresenter.setUpVariablesEditor();
				top.getChildren().add(variablesEditor.getView());

				top.setTopAnchor(variablesEditor.getView(), 0.0);
				top.setBottomAnchor(variablesEditor.getView(), 0.0);
				top.setLeftAnchor(variablesEditor.getView(), 0.0);
				top.setRightAnchor(variablesEditor.getView(), 0.0);

				noFunctionBlockLoadedLabel.setVisible(false);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			System.out.println("File is not valid!");
		}
	}

	public void loadFunctionBlock() {

	}

}
