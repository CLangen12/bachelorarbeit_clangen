\select@language {ngerman}
\contentsline {section}{Abbildungsverzeichnis}{4}{section*.3}
\contentsline {section}{Tabellenverzeichnis}{5}{section*.4}
\contentsline {section}{\numberline {1}Einleitung}{6}{section.1}
\contentsline {subsection}{\numberline {1.1}Motivation}{6}{subsection.1.1}
\contentsline {subsubsection}{\numberline {1.1.1}Entwicklung von Expertensystemen in der Medizin}{6}{subsubsection.1.1.1}
\contentsline {subsubsection}{\numberline {1.1.2}Nutzung von Expertensystemen in der Medizin}{8}{subsubsection.1.1.2}
\contentsline {subsection}{\numberline {1.2}Problemstellung}{8}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Zielsetzung}{9}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Aufbau dieser Arbeit}{9}{subsection.1.4}
\contentsline {section}{\numberline {2}Grundlagen}{10}{section.2}
\contentsline {subsection}{\numberline {2.1}Wissen}{10}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Wissen -- Begriffsbestimmung}{11}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Explizites und implizites Wissen}{12}{subsubsection.2.1.2}
\contentsline {subsection}{\numberline {2.2}Expertensysteme}{12}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Definition}{13}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Expertenwissen und Experten}{14}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}Architektur von Expertensystemen}{15}{subsubsection.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.4}Wissensrepr\IeC {\"a}sentation}{17}{subsubsection.2.2.4}
\contentsline {paragraph}{\nonumberline Regelbasierte Systeme}{18}{section*.7}
\contentsline {paragraph}{\nonumberline Fallbasiertes Schlie\IeC {\ss }en}{18}{figure.caption.10}
\contentsline {paragraph}{\nonumberline K\IeC {\"u}nstliche neuronale Netze}{19}{section*.11}
\contentsline {subsection}{\numberline {2.3}Knowledge Engineering}{19}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Allgemeine Beschreibung}{20}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}Wissensakquisition als Modellbildung}{21}{subsubsection.2.3.2}
\contentsline {subsubsection}{\numberline {2.3.3}Wissenserhebung}{22}{subsubsection.2.3.3}
\contentsline {subsection}{\numberline {2.4}Zusammenfassung}{23}{subsection.2.4}
\contentsline {section}{\numberline {3}Verwandte Arbeiten}{24}{section.3}
\contentsline {subsection}{\numberline {3.1}Literaturauswahl}{24}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Ergebnisse}{25}{subsection.3.2}
\contentsline {section}{\numberline {4}Expertensysteme in der Medizin}{26}{section.4}
\contentsline {subsection}{\numberline {4.1}Expertensysteme in der Medizin}{27}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Motivation -- Anwendung von Expertensystemen in der Medizin}{27}{subsubsection.4.1.1}
\contentsline {paragraph}{\nonumberline Vorteile}{27}{section*.15}
\contentsline {paragraph}{\nonumberline Nachteile}{28}{section*.16}
\contentsline {subsubsection}{\numberline {4.1.2}Geschichte}{29}{subsubsection.4.1.2}
\contentsline {paragraph}{\nonumberline MYCIN}{30}{section*.17}
\contentsline {paragraph}{\nonumberline INTERNIST}{31}{section*.19}
\contentsline {paragraph}{\nonumberline CADUCEUS}{32}{section*.20}
\contentsline {paragraph}{\nonumberline CASNET}{32}{section*.21}
\contentsline {paragraph}{\nonumberline PUFF}{32}{section*.22}
\contentsline {subsubsection}{\numberline {4.1.3}Bewertung der historischen Expertensysteme}{33}{subsubsection.4.1.3}
\contentsline {subsubsection}{\numberline {4.1.4}Anwendungsm\IeC {\"o}glichkeiten von Expertensystemen in der Humanmedizin}{35}{subsubsection.4.1.4}
\contentsline {paragraph}{\nonumberline Diagnoseunterst\IeC {\"u}tzung}{36}{section*.24}
\contentsline {paragraph}{\nonumberline Lehre und Ausbildung}{36}{section*.25}
\contentsline {paragraph}{\nonumberline Weitere Anwendungsm\IeC {\"o}glichkeiten}{36}{section*.26}
\contentsline {subsubsection}{\numberline {4.1.5}Zusammenfassung}{37}{subsubsection.4.1.5}
\contentsline {subsection}{\numberline {4.2}Knowledge Engineering in der Medizin}{37}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Wissensarten in der Medizin}{38}{subsubsection.4.2.1}
\contentsline {paragraph}{\nonumberline Physiologisches und pathophysiologisches Wissen}{38}{section*.27}
\contentsline {paragraph}{\nonumberline Nosologisches Wissen}{38}{section*.28}
\contentsline {paragraph}{\nonumberline \IeC {\"A}tiologisches Wissen}{38}{section*.29}
\contentsline {paragraph}{\nonumberline Pharmakologisches Wissen}{39}{section*.30}
\contentsline {paragraph}{\nonumberline Therapeutisches Wissen}{39}{section*.31}
\contentsline {paragraph}{\nonumberline Diagnostisches Wissen}{39}{section*.32}
\contentsline {paragraph}{\nonumberline Small World Hypothese}{39}{section*.33}
\contentsline {subsubsection}{\numberline {4.2.2}Probleme beim Aufbau einer medizinischen Wissensbasis}{40}{subsubsection.4.2.2}
\contentsline {paragraph}{\nonumberline Abgrenzung:}{42}{section*.35}
\contentsline {subsubsection}{\numberline {4.2.3}Prinzipieller L\IeC {\"o}sungsansatz}{42}{subsubsection.4.2.3}
\contentsline {subsubsection}{\numberline {4.2.4}Zusammenfassung}{44}{subsubsection.4.2.4}
\contentsline {section}{\numberline {5}Knowledge Engineering in der Neurologie}{44}{section.5}
\contentsline {subsection}{\numberline {5.1}Vor\IeC {\"u}berlegungen}{45}{subsection.5.1}
\contentsline {paragraph}{\nonumberline Diagnostik in der Neurologie}{45}{section*.37}
\contentsline {paragraph}{\nonumberline Einsatzzeitpunkt des Expertensystems}{45}{section*.38}
\contentsline {paragraph}{\nonumberline Eingrenzung}{46}{section*.39}
\contentsline {paragraph}{\nonumberline Ziel des Knowledge Engineering}{46}{section*.40}
\contentsline {paragraph}{\nonumberline Abgrenzung}{46}{section*.41}
\contentsline {subsection}{\numberline {5.2}Wissensakquisition}{47}{subsection.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}Wissensquellen selektieren}{47}{subsubsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.2}Wissenserhebung}{48}{subsubsection.5.2.2}
\contentsline {paragraph}{\nonumberline Benigner peripherer paroxysmaler Lagerungsschwindel}{49}{section*.43}
\contentsline {paragraph}{\nonumberline Neuritis Vestibularis}{49}{section*.44}
\contentsline {paragraph}{\nonumberline Bilaterale Vestibulopathie}{50}{section*.45}
\contentsline {paragraph}{\nonumberline Vestibularisparoxysmie}{50}{section*.46}
\contentsline {paragraph}{\nonumberline Morbus Meni\IeC {\`e}re}{50}{section*.47}
\contentsline {paragraph}{\nonumberline Vestibul\IeC {\"a}re Migr\IeC {\"a}ne}{51}{section*.48}
\contentsline {paragraph}{\nonumberline Phobischer Schwankschwindel}{51}{section*.49}
\contentsline {paragraph}{\nonumberline Differentialdiagnose}{51}{section*.50}
\contentsline {paragraph}{\nonumberline Morbus Wilson}{52}{section*.51}
\contentsline {subsubsection}{\numberline {5.2.3}Wissensformalisierung}{53}{subsubsection.5.2.3}
\contentsline {subsubsection}{\numberline {5.2.4}Zusammenfassung Wissensakquisition}{59}{subsubsection.5.2.4}
\contentsline {subsection}{\numberline {5.3}Wissensoperationalisierung}{59}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Evaluation}{60}{subsection.5.4}
\contentsline {subsubsection}{\numberline {5.4.1}1. Fall: "`57-J\IeC {\"a}hrige mit akutem Drehschwindel mit Verst\IeC {\"a}rkung bei Kopfbewegungen"' \cite [S.26]{Fallbuch}}{60}{subsubsection.5.4.1}
\contentsline {subsubsection}{\numberline {5.4.2}Kontrollroutine}{65}{subsubsection.5.4.2}
\contentsline {subsubsection}{\numberline {5.4.3}Nachweis der korrekten Funktionsweise der anderen Funktionsbl\IeC {\"o}cke}{67}{subsubsection.5.4.3}
\contentsline {paragraph}{\nonumberline Neuritis Vestibularis}{67}{section*.72}
\contentsline {paragraph}{\nonumberline Bilaterale Vestibulopathie}{68}{section*.74}
\contentsline {paragraph}{\nonumberline Vestibularisparoxysmie}{68}{section*.76}
\contentsline {paragraph}{\nonumberline Morbus Meni\IeC {\`e}re}{69}{section*.78}
\contentsline {paragraph}{\nonumberline Vestibul\IeC {\"a}re Migr\IeC {\"a}ne}{70}{section*.80}
\contentsline {paragraph}{\nonumberline Phobischer Schwankschwindel}{70}{section*.82}
\contentsline {paragraph}{\nonumberline Morbus Wilson}{71}{section*.84}
\contentsline {subsubsection}{\numberline {5.4.4}Zusammenfassung Evaluation}{71}{subsubsection.5.4.4}
\contentsline {subsection}{\numberline {5.5}Zusammenfassung}{72}{subsection.5.5}
\contentsline {section}{\numberline {6}Fazit}{72}{section.6}
\contentsline {section}{\numberline {7}Ausblick}{74}{section.7}
\contentsline {section}{\numberline {A}Literaturverzeichnis}{76}{appendix.A}
\contentsline {section}{\numberline {B}Funktionsbl\IeC {\"o}cke der Krankheiten}{80}{appendix.B}
\contentsline {subsection}{\numberline {B.1}Benigner peripherer paroxysmaler Lagerungsschwindel}{80}{subsection.B.1}
\contentsline {paragraph}{\nonumberline Linguistische Variablen des Funktionsblocks "`Nystagmus\_of\_benign\_paroxysmal\_type"'}{80}{section*.87}
\contentsline {paragraph}{\nonumberline Funktionsblock "`Nystagmus\_of\_benign\_paroxysmal\_type"'}{80}{section*.90}
\contentsline {subsection}{\numberline {B.2}Neuritis Vestibularis}{82}{subsection.B.2}
\contentsline {paragraph}{\nonumberline Linguistische Variablen des Funktionsblocks "`Neuritis\_Vestibularis"'}{82}{section*.91}
\contentsline {paragraph}{\nonumberline Funktionsblock "`Neuritis\_Vestibularis"'}{83}{section*.94}
\contentsline {subsection}{\numberline {B.3}Bilaterale Vestibulopathie}{86}{subsection.B.3}
\contentsline {paragraph}{\nonumberline Linguistische Variablen des Funktionsblocks "`Bilaterale\_vestibulopathie"'}{86}{section*.95}
\contentsline {paragraph}{\nonumberline Funktionsblock "`Bilaterale\_vestibulopathie"'}{87}{section*.98}
\contentsline {subsection}{\numberline {B.4}Vestibularisparoxysmie}{90}{subsection.B.4}
\contentsline {paragraph}{\nonumberline Linguistische Variablen des Funktionsblocks "`Vestibularisparoxysmie"'}{90}{section*.99}
\contentsline {paragraph}{\nonumberline Funktionsblock "`Vestibularisparoxysmie"'}{90}{section*.102}
\contentsline {subsection}{\numberline {B.5}Morbus Meni\IeC {\`e}re}{92}{subsection.B.5}
\contentsline {paragraph}{\nonumberline Linguistische Variablen des Funktionsblocks "`Morbus\_meniere"'}{92}{section*.103}
\contentsline {paragraph}{\nonumberline Funktionsblock "`Morbus\_meniere"'}{93}{section*.106}
\contentsline {subsection}{\numberline {B.6}Vestibul\IeC {\"a}re Migr\IeC {\"a}ne}{95}{subsection.B.6}
\contentsline {paragraph}{\nonumberline Linguistische Variablen des Funktionsblocks "`Vestibular\_migraine"'}{95}{section*.107}
\contentsline {paragraph}{\nonumberline Funktionsblock "`Vestibular\_migraine"'}{95}{section*.110}
\contentsline {subsection}{\numberline {B.7}Phobischer Schwankschwindel}{98}{subsection.B.7}
\contentsline {paragraph}{\nonumberline Linguistische Variablen des Funktionsblocks "`Phobic\_postural\_vertigo"'}{98}{section*.111}
\contentsline {paragraph}{\nonumberline Funktionsblock "`Phobic\_postural\_vertigo"'}{99}{section*.114}
\contentsline {subsection}{\numberline {B.8}Morbus Wilson}{101}{subsection.B.8}
\contentsline {paragraph}{\nonumberline Linguistische Variablen des Funktionsblocks "`Morbus\_wilson"'}{101}{section*.115}
\contentsline {paragraph}{\nonumberline Funktionsblock "`Morbus\_wilson"'}{101}{section*.118}
\contentsline {section}{\numberline {C}Abschlie\IeC {\ss }ende Erkl\IeC {\"a}rung}{103}{appendix.C}
