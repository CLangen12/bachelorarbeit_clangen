\section{Grundlagen} \label{Kap2}
In diesem Kapitel werden die fachlichen Grundlagen der in dieser Abschlussarbeit behandelten Thematik erläutert. Zu diesem Zweck wird in Kapitel \ref{sec:Wissen} (S.\pageref{sec:Wissen}) zunächst der Begriff \textit{Wissen} definiert. Im Abschnitt \ref{sec:Expertensysteme} werden im Anschluss die wesentlichen Grundlagen von \textit{Expertensystemen} erläutert. Eine Erklärung des \textit{Knowledge Engineering} wird im Kapitel \ref{sec:Knowledge Engineering} vorgenommen. Abschließend wird im Abschnitt \ref{sec:ZSM_Grundlagen} eine Zusammenfassung dieses Kapitels durchgeführt. 


\subsection{Wissen} \label{sec:Wissen}
Wir verwenden das Wort "`Wissen"' beinahe täglich in unserem Sprachgebrauch. Jeder hat eine Vorstellung davon, was das Wort bedeutet. Recherchiert man den Begriff, findet sich keine allgemeingültige Definition, sondern "`eine Fülle von Begriffsdefinitionen, die in Summe einen weiten Interpretationsspielraum zulassen"' \cite[S.13]{KM_Frey}. So verwenden verschiedene wissenschaftliche Disziplinen, wie Psychologie, Sozialwissenschaften und Informatik, unterschiedliche Definitionen für denselben Begriff \cite[S.24]{Reinmann}. Die grundlegende Gemeinsamkeit der Gesamtheit dieser Begriffsdefinitionen liegt darin, dass Wissen an eine Person gebunden ist und lediglich in ihrem Kopf existiert \cite[S.1]{Kunze}.

\subsubsection{Wissen -- Begriffsbestimmung}

 \begin{figure}[h]
    \centering
    \includegraphics[width=210pt]{Wissenspyramide}    
    \caption[Wissenspyramide]{Wissenspyramide (Quelle: Eigene Darstellung in Anlehnung an \cite[S.4]{Krcmar})}
    \label{Wissenspyramide}
  \end{figure}

In der allgemeinen mündlichen und schriftlichen Kommunikation werden nicht selten die Begriffe \textit{Daten}, \textit{Informationen} und \textit{Wissen} als Synonyme verwendet. Im Kontext wissenschaftlicher Betrachtung wird jedoch sprachlich präziser differenziert und jedem Begriff eine spezielle Bedeutung zugewiesen. Der Zusammenhang dieser Begriffe wird in der Literatur häufig in Form einer Pyramide -- auch Wissenspyramide genannt -- veranschaulicht (siehe Abbildung \ref{Wissenspyramide}). Diese vierstufige Pyramide setzt die Begrifflichkeiten \textit{Zeichen}, \textit{Daten}, \textit{Informationen} und \textit{Wissen} zueinander in Beziehung. Auf der untersten Stufe befinden sich die Zeichen. Bei Zeichen handelt es sich um zusammenhanglose Datenelemente \cite[S.1]{Kunze}. "`Daten entstehen, wenn Zeichen (also Buchstaben, Ziffern, Sonderzeichen, Bilder) mit Ordnungsregeln (Syntax) verbunden werden, sodass daraus z. B. Wörter entstehen"' \cite[S.24]{Reinmann}. Wird diesen Daten in einem bestimmten Kontext eine Bedeutung (Semantik) zuteil, so spricht man von Informationen \cite[S.4]{Krcmar}. Bereits dieser Schritt ist nur möglich, wenn eine Person in der Lage ist, den Daten eine entsprechende Bedeutung zuzuweisen. Vernetzt eine Person diese Information mit anderen Informationen, Erfahrungen und Erwartungen, so entsteht daraus Wissen. \cite[S.24]{Reinmann} 

Zur Verdeutlichung der zuvor aufgezeigten Zusammenhänge könnte man beispielsweise die Zeichen "`1"',"`,"',"`2"',"`7"' so zusammensetzen, dass sich daraus "`1,27"' als Datum ergibt. Befindet man sich zudem an einer Tankstelle, lässt sich aus dem Kontext die Bedeutung zuweisen, dass es sich bei dem Datum um einen Kraftstoffpreis handelt. Somit ist eine Information entstanden. Hat man Informationen über die aktuellen Preise an anderen Tankstellen oder Erfahrungen über temporäre Preisentwicklungen und vernetzt diese miteinander, entsteht das Wissen, ob es sich beispielsweise um einen hohen oder niedrigen Preis handelt.

\subsubsection{Explizites und implizites Wissen}
\textit{Wissen} an sich lässt sich prinzipiell in explizites und implizites Wissen unterteilen. \textit{Explizites Wissen} ist einfacher kommunizier- und darstellbar \cite[S.60]{KGoetz}. Das Wissen kann problemlos dokumentiert und danach in Form von Fachbüchern, Tonaufnahmen usw. weitergegeben werden. Somit lässt sich beim expliziten Wissen die Weitergabe des Wissens vom eigentlichen Wissensträger abkoppeln \cite[S.25]{Reinmann}. Zum Beispiel lässt sich die Aussage "`Der Mensch läuft aufrecht auf zwei Beinen"' in einem Fachbuch niederschreiben und weitergeben.

Im Gegensatz dazu gilt Wissen als implizit, falls es nicht direkt artikulierbar ist und in hohem Maße von menschlichen Erfahrungen abhängt \cite[S.25]{Reinmann}. Dieses Wissen lässt sich in der Regel nicht dokumentieren und kann grundsätzlich nur durch direkte Kommunikation weitergegeben werden \cite[S.60]{KGoetz}. Als einfaches Beispiel für \textit{implizites Wissen} könnte der Bewegungsvorgang des Laufens eines Menschen angeführt werden. Jeder durchläuft den Lauflernprozess in der frühen Kindheit und behält als Wissensträger das Lernergebnis ein Leben lang. Dennoch ist kaum jemand in der Lage präzise zu artikulieren, wie das Laufen funktioniert. 

\subsection{Expertensysteme} \label{sec:Expertensysteme}
In diesem Kapitel werden die Grundlagen von wissensbasierten Systemen und Expertensystemen\footnote{Im Verlauf der Arbeit verwendet der Autor den Begriff Expertensystem in diesem Kontext.} thematisiert. Eine Differenzierung der beiden Begrifflichkeiten erfolgt durch die Herkunft des Wissens in der Wissensbasis. Stammt das Wissen der Wissensbasis ausschließlich von Experten, handelt es sich um ein Expertensystem. Zu diesem Zweck wird im Abschnitt \ref{ExDef} zunächst eine allgemeine Definition von Expertensystemen gegeben. Anschließend werden im Kapitel \ref{ExW} die Begriffe \textit{Expertenwissen} und \textit{Experten} erklärt. Im darauffolgenden Kapitel \ref{ExArch} wird die Architektur eines Expertensystems näher erläutert. Abschließend werden im Kapitel \ref{ExWR} die Möglichkeiten zur Wissensrepräsentation in einem Expertensystem aufgezeigt.

\subsubsection{Definition} \label{ExDef}
Damit im weiteren Verlauf dieser Arbeit der Leser den Begriff des \textit{Expertensystems} im Sinne des Autors versteht, erfolgt in diesem Kapitel zunächst eine Begriffsdefinition. Vorweg ist anzumerken, dass eine große Anzahl an Definitionen in der einschlägigen Literatur zu diesem Begriff zu verzeichnen ist, die jedoch nahezu alle gemeinsame Kernaussagen hinsichtlich bestimmter Eigenschaften bzw. Grundfunktionen beinhalten.\footnote{Beispielhaft für die oben genannte Feststellung sollen folgende Definitionen dienen: 

"`Expert systems are software solutions utilizing specialists' knowledge in order to support the decision-making process."' \cite[S.1f]{Survey_Neurology}

"`Ein Expertensystem ist ein Computerprogramm (Hardware und Software), das in einem gegebenen Spezialisierungsbereich menschliche Experten in Bezug auf ihr Wissen und ihre Schlussfolgerungsfähigkeit nachbildet."' \cite[S.12]{MethodenWBS}

 } 

Der Autor hat sich für die Bearbeitung der Fragestellung in dieser Arbeit für folgende, eher breit angelegte Definition entschieden:

\begin{quote}
"`An artificial intelligence (AI) application that uses a base of human expertise for problem solving. Its success is based on the quality of data and rules obtained from the human expert."' \cite{DefEx}
\end{quote}

Daraus ergeben sich mehrere wesentliche Eigenschaften von Expertensystemen. Zunächst einmal wird deutlich, dass Expertensysteme ein Teilgebiet der Künstlichen Intelligenz (\textit{artificial intelligence}) sind. Des Weiteren haben Expertensysteme eine mit menschlichem Expertenwissen gefüllte Basis (\textit{base of human expertise}) und lösen Probleme durch das in der Wissensbasis gespeicherte Wissen (\textit{uses base of human expertise for problem solving}). Darüber hinaus wird auch noch verdeutlicht, dass der Erfolg des Systems fundamental von der Qualität des Expertenwissens abhängt (\textit{Its success is based on the quality of data and rules obtained from the human expert.}).

Neben dieser Definition aus den Anwendungsgebieten ist auch eine technische Definition von Expertensystemen üblich. Der Autor hat sich im Zuge dieser Arbeit für die folgende technische Definition entschieden: 

\begin{quote}
"`Expertensysteme sind als besondere Form von Programmen definierbar, die sich durch die Trennung der anwendungsspezifischen Methoden in der Wissensbank
und der anwendungsunabhängigen Programmsteuerung durch die Inferenzmaschine zur Deduktion logischer Schlußfolgerungen auszeichnen."' \cite[S.2]{SystemeFuerExperten}
\end{quote}

Diese Art der Definition zeichnet sich dadurch aus, dass der wichtigste Aspekt von Expertensystemen die strikte Trennung zwischen der Darstellung des Wissens in der Wissensbasis und der Wissensverarbeitung in einer anwendungsunabhängigen Problemlösungs\-komponente ist. Diese Komponente leitet aus den in der Wissensbasis gespeicherten Regeln Schlussfolgerungen zum gegebenen Problem ab. \cite[S.11]{MethodenWBS} 

Zu beachten bei den beiden Definitionen ist, dass sich diese nicht ausschließen, sondern ergänzen. 

\subsubsection{Expertenwissen und Experten} \label{ExW}
Wie die im vorhergehenden Kapitel genannten Definitionen, so beinhalten die meisten anderen Definitionen von Expertensystemen auch in der einen oder anderen Form \textit{menschliche Experten}. Daraus ergibt sich die Fragestellung, welche speziellen Eigenschaften diese Experten besitzen und wie man diese definieren kann. 
 
Beierle et al. führen dazu folgende Eigenschaften und Fähigkeiten auf:

\begin{quote}
	\begin{itemize}
		\item{"`Experten besitzen überdurchschnittliche Fähigkeiten, Probleme in einem speziellen Gebiet zufrieden stellend zu lösen, selbst wenn diese Probleme keine 			eindeutige Lösung besitzen oder neu auftreten.}
		\item{Experten verwenden heuristisches Wissen, um spezielle Probleme zu lösen, und verwerten ihre Erfahrungen.}
		\item{Experten haben Allgemeinwissen.}
		\item{Sie handeln oft intuitiv richtig, können dann aber ihre Entscheidung nicht begründen.}
		\item{Sie können Probleme unter Verwendung von unvollständigem und unsicherem Wissen lösen.}
		\item{Experten sind selten und teuer.}
		\item{Ihre Leistungsfähigkeit ist nicht konstant, sondern kann nach Tagesverfassung schwanken.}
		\item{Ein Experte ist oft nicht ausreichend (z. B. in gutachterlichen Fragen).}
		\item{Expertenwissen kann verloren gehen."' \cite[S.12]{MethodenWBS}}
	\end{itemize}
\end{quote}

Zu den bereits genannten Punkten kommt hinzu, dass es sich bei Expertenwissen häufig um implizites Wissen handelt, welches oft nicht als solches weitergegeben werden kann \cite[S.12]{MethodenWBS}.

Zusammenfassend kann man einen \textit{Experten} im Kontext dieser Überlegungen wie folgt definieren:

\begin{quote}
"`Als Experten können wir eine Person bezeichnen, die durch lange Fachausbildung und umfassend praktische Erfahrung über besonderes Wissen verfügt."' \cite[S.12]{MethodenWBS}
\end{quote}

Expertenwissen weist einige Unterschiede im Vergleich zu normalem Wissen auf. So ist dieses "`anders strukturiert und qualitativ unterschiedlich bezüglich Inhalt, Qualität, Abstraktion, Verknüpfung von Sachverhalten und Lösungsschritten."' \cite[S.12]{MethodenWBS} Dies liegt vor allem an der Verknüpfung von reinem Fachwissen mit der jahrelangen praktischen Erfahrung eines Experten. \cite[S.12]{MethodenWBS}

\subsubsection{Architektur von Expertensystemen} \label{ExArch}
Wie bereits im Abschnitt \ref{ExDef} angesprochen, ist das wesentliche Merkmal der Architektur eines Expertensystems die Trennung der Wissensbasis von der Inferenzkomponente, also der Komponente, die das Wissen verarbeitet. Durch die Trennung von Wissensbasis und Inferenzkomponente lassen sich zwei für Expertensysteme entscheidende Aspekte realisieren. Zum einen erfolgt eine Trennung zwischen der Problembeschreibung und der Problemlösung und zum anderen lässt sich das Wissen über den Anwendungsbereich direkt ausdrücken. \cite[S.12]{MethodenWBS}



Grundsätzlich besteht ein Expertensystem aus folgenden Komponenten: 
\begin{itemize}
\setlength{\itemsep}{0pt}
	\item{Wissensbasis,}
	\item{Inferenzkomponente,}
	\item{Wissenserwerbskomponente,}
	\item{Erklärungskomponente,}
	\item{Dialogkomponente.}
\end{itemize}
   
Die Verknüpfung bzw. Dependenz der einzelnen Komponenten ist Abbildung \ref{Arichtektur_Expertensystem} zu entnehmen und werden im Verlauf dieses Kapitels näher erläutert. \cite[S.18]{MethodenWBS}

\begin{figure}[h]
    \centering
    \includegraphics[width=400pt]{Schaubild_Expertensystem}    
    \caption[Architektur eines Expertensystems]{Architektur eines Expertensystems (Quelle: Eigene Darstellung in Anlehnung an \cite[S.18]{MethodenWBS} und \cite[S.2]{EXMED})}
    \label{Arichtektur_Expertensystem}
  \end{figure}

\begin{itemize}
\item{Die \textit{Wissensbasis} bildet den Kern jedes Expertensystems \cite[S.2]{EXMED}. In der Wissensbasis wird menschliches Wissen in Form von Regeln gespeichert. Diese Regeln sind häufig WENN-DANN-Regeln (IF-THEN), bei denen eine Bedingung ausgewertet und daraufhin eine Anweisung ausgeführt wird \cite[S.689]{Laudon}. Des Weiteren lässt sich das Wissen in der Wissensbasis in \textit{fallspezifisches} und \textit{regelhaftes} Wissen unterteilen. \textit{Fallspezifisches Wissen} bezieht sich ausschließlich auf den aktuell betrachteten Problemfall, beispielsweise Untersuchungsergebnisse. Es entsteht während der Anwendung des Inferenzalgorithmus oder anderer Komponenten des Systems \cite[S.2]{EXMED}. Das \textit{regelhafte Wissen} umfasst sowohl das Fachwissen aus dem jeweiligen Anwendungsbereich des Expertensystems als auch Allgemeinwissen. Der Umfang, in dem die beiden Arten von Wissen in der Wissensbasis vorhanden sind, kann variieren. \cite[S.17]{MethodenWBS}}

\item{Die \textit{Inferenzkomponente} ist für die Lösung des vorgegebenen Problems verantwortlich. Dazu nutzt sie den Inferenzalgorithmus, der bei der Erstellung des Systems von Experten entwickelt wird und Regeln zur Verarbeitung des Wissens aus der Wissensbasis enthält. Auf der Basis der Falldaten und dem Wissen aus der Wissensbasis leitet der Inferenzalgorithmus Schlussfolgerungen ab und löst so das gegebene Problem. \cite[S.3]{Gaener}} 

\item{Der Aufbau der Wissensbasis erfolgt durch die \textit{Wissenserwerbskomponente}. Diese Komponente unterstützt die Wissensakquisition und die Wissenspflege. Sowohl Experten als auch Wissensingenieure greifen über die Wissenserwerbskomponente auf die Wissensbasis zu. \cite[S.18]{MethodenWBS}}

\item{Die \textit{Erklärungskomponente} ist in der Regel ausschlaggebend für die Akzeptanz eines Expertensystems \cite[S.3]{EXMED}. Der Grund dafür ist, dass das Expertensystem dem Nutzer lediglich das Resultat seiner Schlussfolgerungen präsentiert. Durch die Erklärungskomponente kann sich der Nutzer auf Anfrage die Erklärung der Schlussfolgerung und den Prozess zum gegebenen Resultat anzeigen lassen. \cite[S.18]{MethodenWBS}}

\item{Die \textit{Dialogkomponente} ist verantwortlich für die Kommunikation zwischen den Nutzern und dem Expertensystem. Typischerweise untergliedert man diese Komponente in zwei Schnittstellen. Über die eine Schnittstelle können Experten das System aufbauen, entwickeln und warten und über die andere Schnittstelle können die Anwender des Expertensystems ihre Eingaben machen. \cite[S.18]{MethodenWBS}}
\end{itemize}

Neben den fünf bereits erwähnten Komponenten ist jedoch noch eine weitere Komponente, die Lernkomponente, wünschenswert. Diese kann die Fähigkeit des Lernens nachvollziehen und dadurch automatisch die Wissensbasis verbessern und erweitern. \cite{EnzWINF}
\subsubsection{Wissensrepräsentation} \label{ExWR}
Das Wissen in der Wissensbasis lässt sich auf unterschiedliche Arten repräsentieren. In diesem Kapitel werden drei wesentliche Möglichkeiten zur Darstellung des Wissens aufgezeigt. 

Diese Möglichkeiten sind: 

\begin{itemize}
\setlength{\itemsep}{0pt}
	\item{regelbasierte Systeme,}
	\item{fallbasiertes Schließen,}
	\item{künstliche neuronale Netze.}
\end{itemize}

Im Folgenden werden die einzelnen Möglichkeiten zur Wissensrepräsentation näher erklärt.

\paragraph{Regelbasierte Systeme} 
Bei \textit{regelbasierten Systemen} wird das Wissen in der Wissensbasis durch Regeln gespeichert. Es handelt sich bei diesen Regeln um einfache WENN-DANN-Regeln (IF-THEN). Üblicherweise sind die Regeln der Form "`Wenn A -- Dann B"', das heißt, ist die Prämisse A erfüllt, wird die Konklusion B ausgeführt. Eine einfaches Beispiel für eine solche Regel ist: "`Wenn der Druck (zu) hoch, dann öffne das Ventil"' \cite[S.72]{MethodenWBS}.  Der Vorteil dieser Art der Wissensrepräsentation ist, dass Regeln dieser Form leicht lesbar und verständlich sind. Eine beispielhafte Repräsentation von Wissen in Form von Regeln ist in Abbildung \ref{Regel} dargestellt.

\begin{figure}[h]
    \centering
    \includegraphics[width=400pt]{Schaubild_regelbasierte_Systeme}    
    \caption[Beispiel einer Regel]{Beispiel einer Regel aus einem regelbasierten System (Quelle: \cite[S.15]{EXMED})}
    \label{Regel}
  \end{figure}	


Treten komplexere Problemstellungen auf, kann auch unscharfe Logik (Fuzzy Logik) zur Speicherung der Regeln in der Wissensbasis zum Einsatz kommen. Dadurch wird es möglich differenziertere Lösungen für gegebene Probleme zu finden. \cite[S.3]{Gaener}

\paragraph{Fallbasiertes Schließen}
\begin{figure}[h]
    \centering
    \includegraphics[width=400pt]{Schaubild_fallbasiertes_Schliessen}    
    \caption[Vorgehen beim fallbasiertes Schließen]{Prinzipielles Vorgehen beim fallbasierten Schließen (Quelle: Eigene Darstellung in Anlehnung an \cite[S.1]{CBR})}
    \label{Fallbasiertes_Schliessen}
  \end{figure}	

Die Grundidee des \textit{fallbasierten Schließens} besteht darin, Erfahrungen menschlicher Experten in Form von Fällen in der Wissensbasis zu speichern. Diese Fälle werden dazu eingesetzt, auf der Basis der ursprünglichen Lösung, eine Lösung für das aktuelle Problem zu finden bzw. zu generieren. Das System sucht zu diesem Zweck in der Wissensbasis ähnliche Fälle wie den aktuellen Problemfall und passt die Lösung entsprechend dem Kontext der aktuellen Situation an. Ist diese Lösung erfolgreich, wird auch dieser Fall mit zugehöriger Lösung in der Wissensbasis gespeichert. Das Vorgehen beim \textit{fallbasierten Schließen} ist zudem in Abbildung \ref{Fallbasiertes_Schliessen} visualisiert. Ein Beispiel für die Nutzung von \textit{fallbasiertem Schließem} wäre das Speichern von Fällen im Rechtswesen. Dadurch lassen sich einfach Präzedenzfälle zum gegebenen Rechtsstreit finden. Diese Vorgehensweise ist analog zu der Vorgehensweise eines Menschen. \cite[S.1]{CBR}

\paragraph{Künstliche neuronale Netze}
Bei \textit{künstlichen neuronalen Netzen} wird versucht "`die Bauart biologischer intelligenter Systeme"' \cite[S.67]{MedEx} nachzubilden. 

"`Künstliche Neuronale Netze orientieren sich an einer stark vereinfachten Abstraktion der Funktion natürlicher neuronaler Netze: Baustein eines solchen Netzes sind künstliche Neuronen, die netzartig miteinander verknüpft sind. Längs der Verknüpfungen fließen Informationen."' \cite[S.67]{MedEx}

Typischerweise besteht ein \textit{künstliches neuronales Netz} aus einer Eingabeschicht, einer Ausgabeschicht und einer, vor dem Nutzer verborgenen, Verarbeitungsschicht. Eingabe- und Ausgabeschicht sind verantwortlich für die Kommunikation mit dem Benutzer. Die Verarbeitungsschicht ist verantwortlich für die Logik des Systems. Damit diese Schicht Probleme lösen kann, muss sie vorher von einem Menschen "`trainiert"' werden. Dies geschieht durch Bereitstellen von Trainingsdaten und den entsprechenden Lösungen. Anhand dieser Daten lernt das Netz die Verarbeitung. \cite[S.696]{Laudon}

\subsection{Knowledge Engineering} \label{sec:Knowledge Engineering}
Im folgenden Kapitel wird der Prozess des \textit{Knowledge Engineering} erklärt. Dazu wird im Abschnitt \ref{All_KE} allgemein erklärt was \textit{Knowledge Engineering} bedeutet. Im Kapitel \ref{KE_Metaphern} werden mit der \textit{Mining-View-Metapher} und der \textit{Modeling-View-Metapher} die zwei Ansätze der Wissensakquisition erläutert. 

\subsubsection{Allgemeine Beschreibung} \label{All_KE}
"`Die Bezeichnung \textit{Knowledge Engineering} steht zusammenfassend für das systematische Vorgehen bei der Implementierung, Pflege und Wartung von Wissensbasen."' \cite[S.199]{MedEx} Beim \textit{Knowledge Engineering} geht es um den technischen Umgang mit Wissen und seine technische Verfügbarkeit \cite[S.199]{MedEx}. Neben dem Umgang mit Wissen gehören auch die Entwicklung von Methoden und Werkzeugen zur Unterstützung der Wissenserhebung zum 
\textit{Knowledge Engineering}. \cite[S.161]{KE}

\begin{figure}[h]
    \centering
    \includegraphics[width=400pt]{Schaubild_KE_Teilgebiete}    
    \caption[Teilgebiete Knowledge Engineering]{Teilgebiete des Knowledge Engineering (Quelle: Eigene Darstellung in Anlehnung an \cite[S.200]{MedEx})}
    \label{Schaubild_KE_Teilgebiete}
  \end{figure}

Das \textit{Knowledge Engineering} lässt sich in Teilbereiche gliedern: \textit{Wissensakquisition} und \textit{Wissensoperationalisierung}. 

"`Die \textit{Wissensakquisition} ist der erste Schritt auf dem Weg zur Implementierung eines wissensbasierten Systems. Teilaufgaben bzw. Teilschritte der Wissensakquisition sind die Erhebung von Fachwissen und seine Überführung in eine formale Repräsentation."' \cite[S.199]{MedEx}

Davon unterschieden wird die Phase der \textit{Wissensoperationalisierung}, in welcher der Systementwurf und die Implementierung der wissensverarbeitenden Funktionalität erfolgen. \cite[S.199]{MedEx}

Häufig wird auch die \textit{Evaluation} als Teil des Gesamtprozesses verstanden. In ihr wird die Wissensbasis iterativ an die Anforderungen des Systems angepasst. \cite[S.199]{MedEx}

Der Knowledge Engineering Prozess wird in der Regel von einem \textit{Wissensingenieur} durchgeführt. Dieser ist im Normalfall fachfremd und benötigt die Hilfe von Experten aus dem betrachteten Fachgebiet. \cite[S.200]{MedEx}

\subsubsection{Wissensakquisition als Modellbildung} \label{KE_Metaphern}
"`Bei der Implementierung wissensbasierter Systeme erwies sich die Wissensakquisition als größter Engpass, der auf dem Weg zu einem wissensbasierten System zu passieren ist. Das Gelingen dieses Schrittes hat entscheidenden Einfluss auf die Qualität des Systems; ein Hauptteil des Zeitaufwands und der Kosten entfällt auf diese Phase. Diesen Befund beschrieb E.A. FEIGENBAUM [Hervorhebung im Original] anschaulich: Er nannte die Wissensakquisition den Flaschenhals (bottleneck) der Systementwicklung."' \cite[S.200]{MedEx}

Aufgrund der enormen Bedeutung der Wissensakquisition für den späteren Systemerfolg fand schon frühzeitig die Entwicklung geeigneter Methoden statt. Das Leitbild dieser Überlegungen änderte sich jedoch mit der Zeit grundlegend. \cite[S.200]{MedEx} In den frühen 1980er-Jahren wurde das Knowledge Engineering lediglich als ein Transferprozess angesehen, heutzutage wird Knowledge Engineering hingegen als ein Modellbildungsprozess verstanden. \cite[S.162]{KE} Die beiden Methoden werden häufig in Form der \textit{Mining-View-Metapher} bzw. der \textit{Modeling-View-Metapher} dargestellt \cite[S.200]{MedEx}. In diesem Kapitel werden beide Metaphern näher erläutert. 

Zunächst ging man davon aus, dass es sich beim Knowledge Engineering lediglich um einen Transferprozess handelt. Das benötigte Wissen existiert bereits, muss nur noch gesammelt und in das System übertragen werden. \cite[S.162]{KE} Dadurch entstand die \textit{Mining-View-Metapher}, die ausdrückt, dass das Wissen wie ein Bodenschatz bereits vorhanden ist und nur "`gefördert"' werden muss. \cite[S.200]{MedEx} Meist erhielt man das benötigte Wissen durch Interviews mit Experten. \cite[S.162]{KE} Eine wesentliche Voraussetzung für diese Art des Vorgehens zur Erstellung von Expertensystemen ist, dass ein Experte auf Anfrage sein komplettes Problemlösungsverhalten erklären kann. Ist diese Voraussetzung gegeben, muss das geäußerte Wissen des Experten lediglich in ein geeignetes Repräsentationsformat überführt werden. In der Realität ist diese Voraussetzung jedoch in der Regel nicht erfüllt, da Experten auch implizites Wissen besitzen, das in Teilen eine entscheidende Rolle während der Problemlösung spielt. Es ist beispielsweise so, "`dass in der Medizin ein deutlicher Unterschied zwischen Lehrbuchwissen und dem tatsächlichen diagnostischen und therapeutischen Wissen besteht."' \cite[S.200]{MedEx} Als sich die Bedeutung der Rolle des impliziten Wissens bei der Bearbeitung von Problemen durch den Experten herauskristallisierte, erfolgte ein Paradigmenwechsel. \cite[S.163]{KE}

Betrachtet man den Aufbau eines Expertensystems als einen Modellbildungsprozess (\textit{Modeling-View-Metapher}), wird versucht, ein passgenaues Computermodell zu entwickeln, das die Problemlösungskompetenz eines menschlichen Experten auf seinem Fachgebiet entsprechend nachbildet. Es wird nicht das Ziel verfolgt ein Modell des Experten zu erstellen, sondern lediglich ein Modell, das gleiche Resultate in dem betrachteten Anwendungsgebiet liefert. Um dies adäquat modellieren zu können, muss im Wissensakquisitionsprozess mehr als nur das explizite Wissen gewonnen und im System verarbeitet werden. \cite[S.163]{KE} Da ein Modell etwas nur versuchsweise abbildet, muss der Bereich überprüft und abgesteckt werden, in dem das Modell korrekte Resultate liefert. \cite[S.201]{MedEx} Aus der \textit{Modeling-View-Metapher} ergeben sich folgende Konsequenzen:

\begin{itemize}
\item{Jedes Modell ist nur eine Annäherung an die Realität.}
\item{Der Modellierungsprozess ist ein sich immer wiederholender Prozess, da neue Erkenntnisse im Anwendungsgebiet eine Anpassung des Modells bedeuten können.}
\item{Der Modellierungsprozess ist abhängig von den subjektiven Interpretationen des Wissensingenieurs.} \cite[S.163]{KE}
\end{itemize}

Daraus ergibt sich, dass der Anwender die Ergebnisse immer kritisch beurteilen muss, da bei der Erstellung des Expertensystems Fehler entstehen können. 
Des Weiteren bedeutet es für diese Arbeit, dass die im weiteren Verlauf der Arbeit erstellte Wissensbasis keinesfalls als fertig betrachtet werden kann, sondern immer weiter angepasst werden kann und muss. Weiterhin handelt es sich bei allen Sachverhalten in der Wissensbasis um subjektive Interpretationen des Autors, die eventuell fehlerbehaftet sein können. 

\subsubsection{Wissenserhebung}
Bei der \textit{Wissenserhebung} (knowledge elicitation) geht es um die Erschließung von \textit{Wissensquellen}, um somit Wissen für den Aufbau des Modells verfügbar zu machen. Diese Wissensquellen können sowohl aus explizitem Wissen, beispielsweise aus einem Fachbuch oder der bereits existierenden Wissensbasis eines anderen Systems, als auch aus implizitem Wissen von Experten bestehen. Bei der Erhebung von explizitem Wissen ist zu beachten, dass es laut Arbeitsdefinition (S.\pageref{sec:Wissen}) erforderlich ist, dass ein Mensch das gegebene Dokument interpretiert, um das Wissen zu aktivieren. Eine erfolgreiche Interpretation ist in der Regel abhängig vom Vorwissen auf dem jeweiligen Fachgebiet. So ist es beispielsweise für einen fachfremden Wissensingenieur schwierig, dokumentiertes medizinisches Fachwissen ohne die Unterstützung eines medizinisch Fachkundigen zu verarbeiten. \cite[S.209]{MedEx}

Für die Erhebung von Expertenwissen wurden verschiedene Techniken entwickelt. Entscheidend für die Erhebung ist, welcher der beiden Ansätze bzw. Metaphern zugrunde gelegt wird. Geht man von der \textit{Mining-View-Metapher} aus, kann der Experte sein Wissen problemlos verbalisieren und dokumentieren. In diesem Kontext ist es sinnvoll, dieses Wissen über eine simple Abfrage zu erheben. Dies kann beispielsweise geschehen, indem man den Experten sein Wissen direkt über die \textit{Wissenserwerbskomponente} in das Expertensystem übertragen lässt. \cite[S.209]{MedEx}

Geht man davon aus, dass der Wissensakquisitionsprozess ein Modellbildungsprozess ist, bietet sich die Nutzung folgender Erhebungstechniken an:

\begin{itemize}
\setlength{\itemsep}{0pt}
\item{Protokollanalyse,}
\item{Interview,}
\item{Begriffskonstruktion\footnote{"`Die Überschrift Begriffskonstruktion dient als Sammelbezeichnung. Sie bündelt Verfahren, bei denen besondere Spielregeln zur systematischen Erfassung von Fachbegriffen führen."' \cite[S.209]{MedEx} Darunter fallen zum Beispiel Erhebungstechniken wie \textit{Mind mapping} oder \textit{Concept Laddering}. \cite[S.209]{MedEx}} \cite[S.209]{MedEx}}
\end{itemize}
   
Durch diese Verfahren lässt sich nicht nur der Problemlösungsvorgang dokumentieren und ins System überführen, sondern es kann auch noch notwendiges Vorwissen in den Prozess eingebracht und übernommen werden. So kann man bei einem Interview das fehlende Vorwissen direkt von dem Experten erfragen und erklären lassen. Dies spielt beim Aufbau einer Wissensbasis eine wichtige Rolle, da Experten bei der Problemlösung häufig auf Wissen zurückgreifen, welches ihnen so selbstverständlich erscheint, dass sie dieses nicht erwähnen. An dieser Stelle hat der Wissensingenieur die Möglichkeit nachzufragen. 
   
\subsection{Zusammenfassung} \label{sec:ZSM_Grundlagen}
In diesem Kapitel wurden die wesentlichen technischen und fachlichen Grundlagen dieser Bachelorarbeit dargelegt. Im Abschnitt \ref{sec:Wissen} wurde der Begriff \textit{Wissen} definiert, da dieser im Zusammenhang mit Expertensystemen eine bedeutende Rolle spielt. Im Kapitel \ref{sec:Expertensysteme} wurden die Grundlagen von Expertensystemen erläutert. Dies ist im Verlauf der Arbeit von Bedeutung, da ein grundsätzliches Verständnis dieser Technologie vorliegen muss, damit eine adäquate Darstellung der Anwendungsmöglichkeiten in der Humanmedizin möglich ist. Des Weiteren wird in dieser Arbeit ein Lösungsansatz zur Entwicklung einer Wissensbasis im Fachbereich Neurologie erarbeitet. Dazu musste jedoch der Begriff der Wissensbasis zunächst definiert und in den Gesamtkontext eingeordnet werden. Im Abschnitt \ref{sec:Knowledge Engineering} wird der Prozess des \textit{Knowledge Engineering} aufgearbeitet. Dies ist von Bedeutung, da in einem späteren Kapitel dieser Arbeit beispielhaft eine Wissensbasis für den Fachbereich Neurologie erstellt und dieser Prozess durch Knowledge Engineering vollzogen werden soll. 

Weitere, speziellere Grundlagen werden in den Kapiteln des Hauptteils erläutert, in denen diese benötigt werden. 

In den Grundlagen wird weder bei der Wissensrepräsentation noch bei der Wissenakquisition auf Ontologien\footnote{"`Eine Ontologie ist ein formales Wissensmodell, das im Wissensmanagement, in Experten- und Multiagentensystemen, bei der Informationsintegration und insbesondere im Semantic Web für die Bereitstellung von Wissensstrukturen, für Wissensorganisation oder als Basis der automatisierten Wissensverarbeitung genutzt wird."' \cite{ENZWIONTO}} eingegangen. Der Grund dafür ist, dass diese keine Möglichkeit zum Umgang mit unsicherem und unscharfem Wissen bieten.