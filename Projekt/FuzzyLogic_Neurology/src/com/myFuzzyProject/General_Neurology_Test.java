package com.myFuzzyProject;

import java.util.HashMap;
import java.util.HashSet;

import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.FunctionBlock;
import net.sourceforge.jFuzzyLogic.JFuzzyLogic;
import net.sourceforge.jFuzzyLogic.plot.JFuzzyChart;

public class General_Neurology_Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String filename = "general_Neurology.fcl";
		FIS fis = FIS.load(filename, true);

		if (fis == null) {
			System.err.println("Can't load file: '" + filename + "'");
			System.exit(1);
		}

		// Get default function block
		FunctionBlock fb = fis.getFunctionBlock(null);
		
		//shows fuzzy charts of all variables
		//JFuzzyChart.get().chart(fb);
		
		// Set inputs
		fb.setVariable("rotary_vertigo", 2);
		fb.setVariable("tendency_to_fall", 2);			//left = 1 & right = 2
		fb.setVariable("spontaneous_nystagmus", 1);		//left = 1 & right = 2
		fb.setVariable("unterberger_test", 2);			//left = 1 & right = 2
		fb.setVariable("hearing_loss", 1); 				 //false = 0 & true = 1
		fb.setVariable("vertigo_duration", 1);		 	 //short = 0 - 6 & average = 7 - 23 & long = 25 - 72
		fb.setVariable("tinnitus", 0);			 		//false = 0 & true = 1
		fb.setVariable("kayser_fleischer_ring", 1);
		
		// Evaluate
		fb.evaluate();

		// Show output variable's chart
		fb.getVariable("Neuritis_vestibularis").defuzzify();
		fb.getVariable("Morbus_menerie").defuzzify();
		fb.getVariable("Morbus_wilson").defuzzify();
		

		// Print ruleSet
		//System.out.println(fb);
		System.out.println("Neuritis_vestibularis - Wahrscheinlichkeit: " + fb.getVariable("Neuritis_vestibularis").getValue());
		System.out.println("Morbus_menerie - Wahrscheinlichkeit: " + fb.getVariable("Morbus_menerie").getValue());
		System.out.println("Morbus_Wilson - Wahrscheinlichkeit: " + fb.getVariable("Morbus_wilson").getValue());		
	}

}
