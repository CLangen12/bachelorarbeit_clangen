package com.myFuzzyProject;

import java.util.ArrayList;
import java.util.List;

import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.FunctionBlock;
import net.sourceforge.jFuzzyLogic.plot.JFuzzyChart;

public class FuzzyTestClass {

	public static void main(String[] args) {
		String[] filenames = { "Neuritis_vestibularis",
				"Nystagmus_of_benign_paroxysmal_type", "Morbus_meniere",
				"Vestibularisparoxysmie", "Vestibular_migraine",
				"Morbus_wilson", "Bilaterale_vestibulopathie",
				"Phobic_postural_vertigo", "Stroke", "Huntingtons_disease" };

		List<FunctionBlock> blocks = loadFunctionBlocks(filenames);

		// Get default function block
		// FunctionBlock fb2 = loadFunctionBlocks(filename);

		// Get default function block
		// FunctionBlock fb1 = fis1.getFunctionBlock(null);

		// JFuzzyChart.get().chart(fb);

		// Set inputs for Neuritis Vestibularis
		setInputsForNeuritis_vestibularis(blocks, 2, 9, 1, 2, 5);

		// Set inputs for Nystagmus_of_benign_paroxysmal_type
		/* setInputsForNystagmus_of_benign_paroxysmal_type(blocks, 5, 1, 3); */

		// Set inputs for Morbus_Meniere
		setInputsForMorbus_meniere(blocks, 2, 4, 1, 1);

		// Set inputs for Vestibularisparoxysmie
		setInputsForVestibularisparoxysmie(blocks, 5, 2, 7);

		// Set inputs for Vestibular_migraine
		setInputsForVestibular_migraine(blocks, 2, 5, 1, 1, 0);

		// Set inputs for Morbus_wilson
		setInputsForMorbus_Wilson(blocks, 1);

		// Set inputs for Bilaterale_vestibulopathie
		setInputsForBilaterale_vestibulopathie(blocks, 3, 1, 1, 1, 5, 5);

		// Set inputs for Phobic_postural_vertigo
		setInputsForPhobic_postural_vertigo(blocks, 7, 1, 1, 0);
		
		// Set inputs for Stroke
		setInputsForStroke(blocks, 2, 7, 2, 2, 7, 2, 2);
		
		// Set inputs for Huntingtons_disease
		setInputsForHuntingtons_disease(blocks, 2, 7, 2, 7, 2, 7);

		// for(int i = 0; i < blocks.size(); i++){
		// blocks.get(i).setVariable("vertigo_duration", 1);
		// blocks.get(i).setVariable("rotary_vertigo", 5);
		// blocks.get(i).setVariable("dix_hallpike_test", 5);
		// }
		//
		printDisease(blocks, filenames);

	}

	/**
	 * Loads all FunctionBlocks
	 * 
	 * @param filenames
	 * @return List<FunctionBlock>
	 */
	private static List<FunctionBlock> loadFunctionBlocks(String[] filenames) {
		List<FunctionBlock> blocks = new ArrayList<FunctionBlock>();
		for (String disease : filenames) {
			FIS fis = FIS.load("FCLS/" + disease + ".fcl", true);

			if (fis == null) {
				System.err.println("Can't load file: '" + disease + "'");
				System.exit(1);
			}
			blocks.add(fis.getFunctionBlock(null));
		}
		return blocks;
	}

	/**
	 * Prints out all the possibility of all diseases
	 * 
	 * @param blocks
	 * @param filenames
	 */
	private static void printDisease(List<FunctionBlock> blocks,
			String[] filenames) {
		int i = 0;
		for (String disease : filenames) {

			blocks.get(i).getVariable(disease);
			blocks.get(i).evaluate();
			System.out.println(disease + " - Wahrscheinlichkeit: "
					+ blocks.get(i).getVariable(disease).getValue());
			i++;
		}

	}

	/**
	 * Sets the inputs for the FunctionBlock Neuritis_vestibularis
	 * 
	 * @param blocks
	 * @param rotary_vertigo
	 * @param vertigo_duration
	 * @param tendency_to_fall
	 * @param spontaneous_nystagmus
	 * @param unterberger_test
	 */
	private static void setInputsForNeuritis_vestibularis(
			List<FunctionBlock> blocks, double rotary_vertigo,
			double vertigo_duration, double tendency_to_fall,
			double spontaneous_nystagmus, double unterberger_test) {
		blocks.get(0).setVariable("rotary_vertigo", rotary_vertigo);
		blocks.get(0).setVariable("vertigo_duration", vertigo_duration);
		blocks.get(0).setVariable("tendency_to_fall", tendency_to_fall);
		blocks.get(0).setVariable("spontaneous_nystagmus",
				spontaneous_nystagmus);
		blocks.get(0).setVariable("unterberger_test", unterberger_test);
	}

	/**
	 * Sets the inputs for the FunctionBlock Nystagmus_of_benign_paroxysmal_type
	 * 
	 * @param blocks
	 * @param rotary_vertigo
	 * @param vertigo_duration
	 * @param dix_hallpike_test
	 * @param dix_hallpike_test_result
	 */
	private static void setInputsForNystagmus_of_benign_paroxysmal_type(
			List<FunctionBlock> blocks, double rotary_vertigo,
			double vertigo_duration, double dix_hallpike_test) {
		blocks.get(1).setVariable("rotary_vertigo", rotary_vertigo);
		blocks.get(1).setVariable("vertigo_duration", vertigo_duration);
		blocks.get(1).setVariable("dix_hallpike_test", dix_hallpike_test);
	}

	/**
	 * Sets the inputs for the FunctionBlock Morbus_meniere
	 * 
	 * @param blocks
	 * @param rotary_vertigo
	 * @param vertigo_duration
	 * @param tinnitus
	 * @param hearing_loss
	 */
	private static void setInputsForMorbus_meniere(List<FunctionBlock> blocks,
			double rotary_vertigo, double vertigo_duration, double tinnitus,
			double hearing_loss) {
		blocks.get(2).setVariable("rotary_vertigo", rotary_vertigo);
		blocks.get(2).setVariable("vertigo_duration", vertigo_duration);
		blocks.get(2).setVariable("tinnitus", tinnitus);
		blocks.get(2).setVariable("hearing_loss", hearing_loss);
	}

	/**
	 * Sets the inputs for the FunctionBlock Vestibularisparoxysmie
	 * 
	 * @param blocks
	 * @param rotary_vertigo
	 * @param vertigo_duration
	 * @param triggering_though_head_position_or_hyperventilation
	 */
	private static void setInputsForVestibularisparoxysmie(
			List<FunctionBlock> blocks, double rotary_vertigo,
			double vertigo_duration,
			double triggering_though_head_position_or_hyperventilation) {
		blocks.get(3).setVariable("rotary_vertigo", rotary_vertigo);
		blocks.get(3).setVariable("vertigo_duration", vertigo_duration);
		blocks.get(3).setVariable(
				"triggering_though_head_position_or_hyperventilation",
				triggering_though_head_position_or_hyperventilation);
	}

	/**
	 * Sets the inputs for the FunctionBlock Vestibular_migraine
	 * 
	 * @param blocks
	 * @param rotary_vertigo
	 * @param vertigo_duration
	 * @param headache
	 * @param oscillopsia
	 * @param sensivity
	 */
	private static void setInputsForVestibular_migraine(
			List<FunctionBlock> blocks, double rotary_vertigo,
			double vertigo_duration, double headache, double oscillopsia,
			double sensivity) {
		blocks.get(4).setVariable("rotary_vertigo", rotary_vertigo);
		blocks.get(4).setVariable("vertigo_duration", vertigo_duration);
		blocks.get(4).setVariable("headache", headache);
		blocks.get(4).setVariable("oscillopsia", oscillopsia);
		blocks.get(4).setVariable("sensivity", sensivity);
	}

	/**
	 * Sets the inputs for the FunctionBlock Morbus_wilson
	 * 
	 * @param blocks
	 * @param kayser_fleischer_ring
	 */
	private static void setInputsForMorbus_Wilson(List<FunctionBlock> blocks,
			double kayser_fleischer_ring) {
		blocks.get(5).setVariable("kayser_fleischer_ring",
				kayser_fleischer_ring);
	}

	/**
	 * Sets the inputs for the FunctionBlock Bilaterale_vestibulopathie
	 * 
	 * @param blocks
	 * @param staggering_vertigo
	 * @param oscillopsia
	 * @param ataxia
	 * @param tendency_to_fall_without_visual_system
	 * @param halmagyi_test
	 * @param caloric_vestibular_test
	 */
	private static void setInputsForBilaterale_vestibulopathie(
			List<FunctionBlock> blocks, double staggering_vertigo,
			double oscillopsia, double ataxia,
			double tendency_to_fall_without_visual_system,
			double halmagyi_test, double caloric_vestibular_test) {
		blocks.get(6).setVariable("staggering_vertigo", staggering_vertigo);
		blocks.get(6).setVariable("oscillopsia", oscillopsia);
		blocks.get(6).setVariable("ataxia", ataxia);
		blocks.get(6).setVariable("tendency_to_fall_without_visual_system",
				tendency_to_fall_without_visual_system);
		blocks.get(6).setVariable("halmagyi_test", halmagyi_test);
		blocks.get(6).setVariable("caloric_vestibular_test",
				caloric_vestibular_test);
	}

	/**
	 * Sets the inputs for the FunctionBlock Phobic_postural_vertigo
	 * 
	 * @param blocks
	 * @param staggering_vertigo
	 * @param ataxia
	 * @param recovery_though_alcohol_or_sport
	 * @param organic_reasons
	 */
	private static void setInputsForPhobic_postural_vertigo(
			List<FunctionBlock> blocks, double staggering_vertigo,
			double ataxia, double recovery_though_alcohol_or_sport,
			double organic_reasons) {
		blocks.get(7).setVariable("staggering_vertigo", staggering_vertigo);
		blocks.get(7).setVariable("ataxia", ataxia);
		blocks.get(7).setVariable("recovery_though_alcohol_or_sport",
				recovery_though_alcohol_or_sport);
		blocks.get(7).setVariable("organic_reasons", organic_reasons);
	}

	/**
	 * Sets the inputs for the FunctionBlock Stroke
	 * 
	 * @param blocks
	 * @param ataxia
	 * @param vertigo
	 * @param hemiparesis
	 * @param motor_aphasia
	 * @param visual_disturbance
	 * @param unconsciousness
	 * @param sudden_heavy_headache
	 */
	private static void setInputsForStroke(List<FunctionBlock> blocks,
			double ataxia, double vertigo, double hemiparesis,
			double motor_aphasia, double visual_disturbance,
			double unconsciousness, double sudden_heavy_headache) {
		blocks.get(8).setVariable("ataxia", ataxia);
		blocks.get(8).setVariable("vertigo", vertigo);
		blocks.get(8).setVariable("hemiparesis", hemiparesis);
		blocks.get(8).setVariable("motor_aphasia", motor_aphasia);
		blocks.get(8).setVariable("visual_disturbance", visual_disturbance);
		blocks.get(8).setVariable("unconsciousness", unconsciousness);
		blocks.get(8).setVariable("sudden_heavy_headache",
				sudden_heavy_headache);
	}
	
	/**
	 * Sets the inputs for the FunctionBlock Huntingtons_disease
	 * 
	 * @param blocks
	 * @param hyperkinesia
	 * @param depression
	 * @param dementia
	 * @param dysphagia
	 * @param dysarthrophonia
	 * @param change_of_personality
	 */
	private static void setInputsForHuntingtons_disease(List<FunctionBlock> blocks,
			double hyperkinesia, double depression, double dementia,
			double dysphagia, double dysarthrophonia,
			double change_of_personality){
		blocks.get(9).setVariable("hyperkinesia", hyperkinesia);
		blocks.get(9).setVariable("depression", depression);
		blocks.get(9).setVariable("dementia", dementia);
		blocks.get(9).setVariable("dysphagia", dysphagia);
		blocks.get(9).setVariable("dysarthrophonia", dysarthrophonia);
		blocks.get(9).setVariable("change_of_personality", change_of_personality);
	}

}
