package com.myFuzzyProject;

import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.FunctionBlock;

public class Test_Neuritis_vestibularis {

	public static void main(String[] args) {
		String filename = "Neuritis_vestibularis.fcl";
		FIS fis = FIS.load(filename, true);

		if (fis == null) {
			System.err.println("Can't load file: '" + filename + "'");
			System.exit(1);
		}

		// Get default function block
		FunctionBlock fb = fis.getFunctionBlock(null);

		//JFuzzyChart.get().chart(fb);
		
		// Set inputs
		fb.setVariable("rotary_vertigo", 2);
		fb.setVariable("vertigo_duration", 9);
		fb.setVariable("tendency_to_fall", 2);
		fb.setVariable("spontaneous_nystagmus", 1);
		fb.setVariable("unterberger_test", 2);

		
		// Evaluate
		fb.evaluate();

		// Show output variable's chart
		fb.getVariable("Neuritis_vestibularis");
		fb.getVariable("affected_side");

		// Print ruleSet
		//System.out.println(fb);
		
		System.out.println("Neuritis Vestibularis - Wahrscheinlichkeit: " + fb.getVariable("Neuritis_vestibularis").getValue());
		System.out.println("Neuritis Vestibularis - betroffene Seite: " + fb.getVariable("affected_side").getValue());


	}

}
