package com.myFuzzyProject;

import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.FunctionBlock;
import net.sourceforge.jFuzzyLogic.plot.JFuzzyChart;

public class Test_Morbus_wilson {

	public static void main(String[] args) {
		String filename = "Vestibularisparoxysmie.fcl";
		FIS fis = FIS.load(filename, true);

		if (fis == null) {
			System.err.println("Can't load file: '" + filename + "'");
			System.exit(1);
		}

		// Get default function block
		FunctionBlock fb = fis.getFunctionBlock(null);

		fb.setVariable("vertigo_duration", 1);
		fb.setVariable("rotary_vertigo", 5);
		fb.setVariable("one_sided_tinnitus", 1);
		fb.setVariable("one_sided_hearing_loss", 1);
		
		// Evaluate
		fb.evaluate();

		// Show output variable's chart
		fb.getVariable("Vestibularisparoxysmie").defuzzify();
		

		// Print ruleSet
		//System.out.println(fb);
		System.out.println("Vestibularisparoxysmie: " + fb.getVariable("Vestibularisparoxysmie").getValue());


	}

}
