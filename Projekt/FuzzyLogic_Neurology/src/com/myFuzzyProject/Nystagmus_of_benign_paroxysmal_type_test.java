package com.myFuzzyProject;

import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.FunctionBlock;
import net.sourceforge.jFuzzyLogic.plot.JFuzzyChart;

public class Nystagmus_of_benign_paroxysmal_type_test {

	public static void main(String[] args) {
		String filename = "Nystagmus_of_benign_paroxysmal_type.fcl";
		FIS fis = FIS.load(filename, true);

		if (fis == null) {
			System.err.println("Can't load file: '" + filename + "'");
			System.exit(1);
		}

		// Get default function block
		FunctionBlock fb = fis.getFunctionBlock(null);

		//JFuzzyChart.get().chart(fb);
		
		// Set inputs
		fb.setVariable("rotary_vertigo", 5);
		fb.setVariable("vertigo_duration", 1);
		fb.setVariable("dix_hallpike_test", 1);
		fb.setVariable("dix_hallpike_test_result", 2);
		
		// Evaluate
		fb.evaluate();

		// Show output variable's chart
		fb.getVariable("Nystagmus_of_benign_paroxysmal_type");
		fb.getVariable("affected_side");

		// Print ruleSet
		//System.out.println(fb);
		
		System.out.println("Nystagmus of benign paroxysmal type - Wahrscheinlichkeit: " + fb.getVariable("Nystagmus_of_benign_paroxysmal_type").getValue());
		System.out.println("Nystagmus of benign paroxysmal type - betroffene Seite: " + fb.getVariable("affected_side").getValue());

	}

}
