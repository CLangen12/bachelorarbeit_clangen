package FuzzyLiteNeurology;

import com.fuzzylite.Engine;
import com.fuzzylite.FuzzyLite;
import com.fuzzylite.Op;
import com.fuzzylite.defuzzifier.Centroid;
import com.fuzzylite.norm.s.Maximum;
import com.fuzzylite.norm.t.Minimum;
import com.fuzzylite.rule.Rule;
import com.fuzzylite.rule.RuleBlock;
import com.fuzzylite.term.Triangle;
import com.fuzzylite.variable.InputVariable;
import com.fuzzylite.variable.OutputVariable;



public class Main {

    public static void main(String[] args) {

        Engine engine = new Engine();
        engine.setName("Neuritisvestibularis");

        InputVariable drehschwindelInput = new InputVariable();
        drehschwindelInput.setEnabled(true);
        drehschwindelInput.setName("Dreschwindel");
        drehschwindelInput.setRange(0.000, 10.000);
        drehschwindelInput.addTerm(new Triangle("ploetzlich", 0.000, 2.000, 4.000));
        drehschwindelInput.addTerm(new Triangle("attacke", 2.000, 5.000, 8.000));
        drehschwindelInput.addTerm(new Triangle("konstant", 6.000, 8.000, 10.000));
        engine.addInputVariable(drehschwindelInput);

        InputVariable schwindelDauerInput = new InputVariable();
        schwindelDauerInput.setEnabled(true);
        schwindelDauerInput.setName("Schwindeldauer");
        schwindelDauerInput.setRange(0.040, 10.020);
        schwindelDauerInput.addTerm(new Triangle("kurz", 0.000, 2.000, 4.000));
        schwindelDauerInput.addTerm(new Triangle("mittel", 2.000, 5.000, 8.000));
        schwindelDauerInput.addTerm(new Triangle("lang", 6.000, 8.000, 10.000));
        engine.addInputVariable(schwindelDauerInput);

        InputVariable fallneigungInput = new InputVariable();
        fallneigungInput.setEnabled(true);
        fallneigungInput.setName("Falleneigung");
        fallneigungInput.setRange(0.000, 10.000);
        fallneigungInput.addTerm(new Triangle("links", 0.000, 3.500, 7.000));
        fallneigungInput.addTerm(new Triangle("rechts", 3.500, 7.000, 10.000));
        engine.addInputVariable(fallneigungInput);

        InputVariable spontannystagmusInput = new InputVariable();
        spontannystagmusInput.setEnabled(true);
        spontannystagmusInput.setName("Spontannystagmus");
        spontannystagmusInput.setRange(0.000, 10.000);
        spontannystagmusInput.addTerm(new Triangle("links", 0.000, 3.500, 7.000));
        spontannystagmusInput.addTerm(new Triangle("rechts", 3.500, 7.000, 10.000));
        engine.addInputVariable(spontannystagmusInput);

        InputVariable unterbergerInput = new InputVariable();
        unterbergerInput.setEnabled(true);
        unterbergerInput.setName("Unterberger_Versuch");
        unterbergerInput.setRange(0.000, 8.000);
        unterbergerInput.addTerm(new Triangle("negativ", 2.000, 3.000, 4.000));
        unterbergerInput.addTerm(new Triangle("links", 4.000, 5.000, 6.000));
        unterbergerInput.addTerm(new Triangle("nicht_ausgefuehrt", 0.000, 1.000, 2.000));
        unterbergerInput.addTerm(new Triangle("rechts", 6.000, 7.000, 8.000));
        engine.addInputVariable(unterbergerInput);

        OutputVariable outputVariable = new OutputVariable();
        outputVariable.setEnabled(true);
        outputVariable.setName("Neuritis_vestibularis");
        outputVariable.setRange(0.000, 10.000);
        outputVariable.fuzzyOutput().setAccumulation(new Maximum());
        outputVariable.setDefuzzifier(new Centroid(200));
        outputVariable.setDefaultValue(0.000);
        outputVariable.setLockPreviousOutputValue(false);
        outputVariable.setLockOutputValueInRange(false);
        outputVariable.addTerm(new Triangle("unwahrscheinlich", 0.000, 2.000, 4.000));
        outputVariable.addTerm(new Triangle("moeglich", 2.000, 5.000, 8.000));
        outputVariable.addTerm(new Triangle("wahrscheinlich", 6.000, 8.000, 10.000));
        engine.addOutputVariable(outputVariable);

        RuleBlock ruleBlock = new RuleBlock();
        ruleBlock.setEnabled(true);
        ruleBlock.setName("");
        ruleBlock.setConjunction(new Minimum());
        ruleBlock.setDisjunction(new Maximum());
        ruleBlock.setActivation(new Minimum());
        ruleBlock.addRule(Rule.parse("if Dreschwindel is ploetzlich and Schwindeldauer is lang and Falleneigung is links and Spontannystagmus is rechts and Unterberger_Versuch is links then Neuritis_vestibularis is wahrscheinlich", engine));
        ruleBlock.addRule(Rule.parse("if Dreschwindel is ploetzlich and Schwindeldauer is lang and Falleneigung is rechts and Spontannystagmus is links and Unterberger_Versuch is rechts then Neuritis_vestibularis is wahrscheinlich", engine));
        ruleBlock.addRule(Rule.parse("if Dreschwindel is attacke and Schwindeldauer is mittel and Falleneigung is links and Spontannystagmus is rechts and Unterberger_Versuch is links then Neuritis_vestibularis is moeglich", engine));
        ruleBlock.addRule(Rule.parse("if Dreschwindel is attacke and Schwindeldauer is mittel and Falleneigung is rechts and Spontannystagmus is links and Unterberger_Versuch is rechts then Neuritis_vestibularis is moeglich", engine));
        ruleBlock.addRule(Rule.parse("if Dreschwindel is ploetzlich and Schwindeldauer is lang and Falleneigung is links and Spontannystagmus is rechts and Unterberger_Versuch is nicht_ausgefuehrt then Neuritis_vestibularis is moeglich", engine));
        ruleBlock.addRule(Rule.parse("if Dreschwindel is ploetzlich and Schwindeldauer is lang and Falleneigung is rechts and Spontannystagmus is links and Unterberger_Versuch is nicht_ausgefuehrt then Neuritis_vestibularis is moeglich", engine));
        ruleBlock.addRule(Rule.parse("if Dreschwindel is konstant or Schwindeldauer is kurz or  Unterberger_Versuch is negativ then Neuritis_vestibularis is unwahrscheinlich", engine));
        engine.addRuleBlock(ruleBlock);

        drehschwindelInput.setInputValue(2.0);
        schwindelDauerInput.setInputValue(8.0);
        fallneigungInput.setInputValue(3.5);
        spontannystagmusInput.setInputValue(7.0);
        unterbergerInput.setInputValue(5.0);
        
        engine.process();
        //System.out.println(ruleBlock.toString());
        System.out.println(outputVariable.getOutputValue());
        //FuzzyLite.logger().info(String.format(outputVariable.getName()+ " " + Op.str(outputVariable.getOutputValue())));
        
        for(RuleBlock rb : engine.getRuleBlocks()) {
            for(Rule rule : rb.getRules()) {
                System.out.println(rule.getText() + " " + rule.activationDegree(rb.getConjunction(),rb.getDisjunction()));
            }
        }


        
    }
}
