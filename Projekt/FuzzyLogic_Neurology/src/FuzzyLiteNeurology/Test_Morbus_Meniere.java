package FuzzyLiteNeurology;

import com.fuzzylite.Engine;
import com.fuzzylite.FuzzyLite;
import com.fuzzylite.Op;
import com.fuzzylite.defuzzifier.Centroid;
import com.fuzzylite.norm.s.Maximum;
import com.fuzzylite.norm.t.Minimum;
import com.fuzzylite.rule.Rule;
import com.fuzzylite.rule.RuleBlock;
import com.fuzzylite.term.Trapezoid;
import com.fuzzylite.term.Triangle;
import com.fuzzylite.variable.InputVariable;
import com.fuzzylite.variable.OutputVariable;

public class Test_Morbus_Meniere {

	public static void main(String[] args) {
		
		Engine engine = new Engine();
		engine.setName("Morbus_Meniere");

		InputVariable drehschwindelInput = new InputVariable();
		drehschwindelInput.setEnabled(true);
		drehschwindelInput.setName("rotary_vertigo");
		drehschwindelInput.setRange(0.000, 10.020);
		drehschwindelInput.addTerm(new Triangle("sudden", 0.000, 2.000, 4.000));
		drehschwindelInput.addTerm(new Trapezoid("attack", 2.000, 4.000, 6.000, 8.000));
		drehschwindelInput.addTerm(new Trapezoid("constant", 6.000, 8.000, 10.000, 10.000));
		engine.addInputVariable(drehschwindelInput);

		InputVariable hoerverlust = new InputVariable();
		hoerverlust.setEnabled(true);
		hoerverlust.setName("hearing_loss");
		hoerverlust.setRange(0.000, 10.000);
		hoerverlust.addTerm(new Triangle("false", 0.000, 0.000, 5.000));
		hoerverlust.addTerm(new Triangle("true", 5.000, 10.000, 10.000));
		engine.addInputVariable(hoerverlust);

		InputVariable tinnitus = new InputVariable();
		tinnitus.setEnabled(true);
		tinnitus.setName("tinnitus");
		tinnitus.setRange(0.000, 10.020);
		tinnitus.addTerm(new Triangle("false", 0.000, 0.000, 5.000));
		tinnitus.addTerm(new Triangle("true", 5.000, 10.000, 10.000));
		engine.addInputVariable(tinnitus);

		InputVariable schwindelDauerInput = new InputVariable();
		schwindelDauerInput.setEnabled(true);
		schwindelDauerInput.setName("vertigo_duration");
		schwindelDauerInput.setRange(0.000, 10.000);
		schwindelDauerInput.addTerm(new Triangle("short", 0.000, 2.000, 4.000));
		schwindelDauerInput.addTerm(new Trapezoid("medium", 2.000, 4.000, 6.000, 8.000));
		schwindelDauerInput.addTerm(new Trapezoid("long", 6.000, 8.000, 10.000, 10.000));
		engine.addInputVariable(schwindelDauerInput);

		OutputVariable outputVariable = new OutputVariable();
		outputVariable.setEnabled(true);
		outputVariable.setName("Morbus_Meniere");
		outputVariable.setRange(0.000, 9.000);
		outputVariable.fuzzyOutput().setAccumulation(new Maximum());
		outputVariable.setDefuzzifier(new Centroid(200));
		outputVariable.setDefaultValue(Double.NaN);
		outputVariable.setLockPreviousOutputValue(false);
		outputVariable.setLockOutputValueInRange(false);
		outputVariable.addTerm(new Triangle("unlikely", 0.000, 0.000, 4.000));
		outputVariable.addTerm(new Trapezoid("possible", 1.000, 4.000, 6.000, 9.000));
		outputVariable.addTerm(new Triangle("likely", 6.000, 9.000, 9.000));
		engine.addOutputVariable(outputVariable);

		RuleBlock ruleBlock = new RuleBlock();
		ruleBlock.setEnabled(true);
		ruleBlock.setName("Morbus_Meniere_No1");
		ruleBlock.setConjunction(new Minimum());
		ruleBlock.setDisjunction(new Maximum());
		ruleBlock.setActivation(new Minimum());
		ruleBlock.addRule(Rule.parse("if rotary_vertigo is sudden and hearing_loss is true and tinnitus is true and vertigo_duration is medium then Morbus_Meniere is likely", engine));
		ruleBlock.addRule(Rule.parse("if rotary_vertigo is attack and hearing_loss is true and tinnitus is true and vertigo_duration is medium then Morbus_Meniere is possible", engine));
		ruleBlock.addRule(Rule.parse("if rotary_vertigo is sudden and hearing_loss is true and tinnitus is true and vertigo_duration is short then Morbus_Meniere is possible", engine));
		ruleBlock.addRule(Rule.parse("if rotary_vertigo is sudden and hearing_loss is true and tinnitus is false and vertigo_duration is medium then Morbus_Meniere is possible", engine));
		ruleBlock.addRule(Rule.parse("if rotary_vertigo is sudden and hearing_loss is false and tinnitus is true and vertigo_duration is medium then Morbus_Meniere is possible", engine));
		ruleBlock.addRule(Rule.parse("if rotary_vertigo is constant or vertigo_duration is long then Morbus_Meniere is unlikely", engine));
		ruleBlock.addRule(Rule.parse("if hearing_loss is false and tinnitus is false then Morbus_Meniere is unlikely", engine));
		engine.addRuleBlock(ruleBlock);

		drehschwindelInput.setInputValue(2.0);
        schwindelDauerInput.setInputValue(2.0);
        hoerverlust.setInputValue(7.0);
        tinnitus.setInputValue(7.0);
		
		engine.process();
	    FuzzyLite.logger().info(String.format(outputVariable.getName()+ " " + Op.str(outputVariable.getOutputValue())));
		
	}

}
