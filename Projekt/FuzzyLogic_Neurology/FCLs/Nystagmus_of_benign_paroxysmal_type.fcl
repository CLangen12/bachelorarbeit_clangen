FUNCTION_BLOCK Nystagmus_of_benign_paroxysmal_type

//--------------------------------------- Define input variables -------------------------------------------

VAR_INPUT

rotary_vertigo : REAL;
dix_hallpike_test : REAL;
vertigo_duration : REAL;

END_VAR

//--------------------------------------- Define output variables -------------------------------------------

VAR_OUTPUT

Nystagmus_of_benign_paroxysmal_type : REAL;

END_VAR

//--------------------------------------- Fuzzify input variables -------------------------------------------

FUZZIFY rotary_vertigo
TERM sudden := (0, 1) (2, 1) (4, 0) ;
TERM attack := (2, 0) (4, 1) (6, 1) (8, 0);
TERM constant := (6, 0) (8, 1) (10, 1);

END_FUZZIFY //rotary_vertigo

FUZZIFY vertigo_duration
TERM short := (0,1)(2,1)(4,0);
TERM medium := (2,0)(4,1)(6,1)(8,0);
TERM long := (6,0)(8,1)(10,1);

END_FUZZIFY //vertigo_duration

FUZZIFY dix_hallpike_test
TERM not_executed := (0,0)(1,1)(2,0); 
TERM negative := (2,0)(3,1)(4,0);
TERM positive := (4,0)(5,1)(6,0);

END_FUZZIFY //dix_hallpike_test

//--------------------------------------- Defuzzify output variables -------------------------------------------

DEFUZZIFY Nystagmus_of_benign_paroxysmal_type
TERM unlikely := (0, 1) (4, 0);
TERM possible := (1, 0) (4,1) (6,1) (9,0);
TERM likely := (6, 0) (9, 1);

METHOD : COG;

DEFAULT := 0;

END_DEFUZZIFY //Nystagmus_of_benign_paroxysmal_type

//--------------------------------------- Define ruleblocks ---------------------------------------------------

RULEBLOCK Nystagmus_of_benign_paroxysmal_type_No1
AND : MIN;
ACT : MIN;
ACCU : MAX;

RULE 1 : IF vertigo_duration IS short AND rotary_vertigo IS attack AND dix_hallpike_test IS positive THEN Nystagmus_of_benign_paroxysmal_type IS likely;
RULE 2 : IF vertigo_duration IS short AND rotary_vertigo IS attack AND dix_hallpike_test IS not_executed THEN Nystagmus_of_benign_paroxysmal_type IS possible;
RULE 3 : IF rotary_vertigo IS attack AND vertigo_duration IS medium AND dix_hallpike_test IS positive THEN Nystagmus_of_benign_paroxysmal_type IS possible;
RULE 4 : IF rotary_vertigo IS constant OR vertigo_duration IS long OR dix_hallpike_test IS negative THEN Nystagmus_of_benign_paroxysmal_type IS unlikely;

END_RULEBLOCK

END_FUNCTION_BLOCK