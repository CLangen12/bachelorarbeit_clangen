FUNCTION_BLOCK Neuritis_vestibularis

//--------------------------------------- Define input variables -------------------------------------------

VAR_INPUT

rotary_vertigo : REAL;
tendency_to_fall : REAL;
spontaneous_nystagmus : REAL;
unterberger_test : REAL;
vertigo_duration : REAL;

END_VAR

//--------------------------------------- Define output variables -------------------------------------------

VAR_OUTPUT

Neuritis_vestibularis : REAL;

END_VAR

//--------------------------------------- Fuzzify input variables -------------------------------------------

FUZZIFY rotary_vertigo
TERM sudden := (0, 1) (2, 1) (4, 0) ;
TERM attack := (2, 0) (4, 1) (6, 1) (8, 0);
TERM constant := (6, 0) (8, 1) (10, 1);

END_FUZZIFY

FUZZIFY tendency_to_fall
TERM left := 1;
TERM right := 2;

END_FUZZIFY

FUZZIFY spontaneous_nystagmus
TERM left := 1;
TERM right := 2;

END_FUZZIFY

FUZZIFY unterberger_test
TERM not_executed := (0,0)(1,1)(2,0);
TERM negative := (2,0)(3,1)(4,0);
TERM positive_left := (4,0)(5,1)(6,0);
TERM positive_right := (6,0)(7,1)(8,0);

END_FUZZIFY

FUZZIFY vertigo_duration
TERM short := (0,1)(2,1)(4,0);
TERM medium := (2,0)(4,1)(6,1)(8,0);
TERM long := (6,0)(8,1)(10,1);

END_FUZZIFY //vertigo_duration

//--------------------------------------- Defuzzify output variables -------------------------------------------

DEFUZZIFY Neuritis_vestibularis
TERM unlikely := (0, 1) (4, 0) ;
TERM possible := (1, 0) (4,1) (6,1) (9,0);
TERM likely := (6, 0) (9, 1);

METHOD : COG;

DEFAULT := 0;

END_DEFUZZIFY

//--------------------------------------- Define ruleblocks ---------------------------------------------------

RULEBLOCK Neuritis_vestibularis_No1
AND : MIN;
ACT : MIN;
ACCU : MAX;

RULE 1 : IF rotary_vertigo IS sudden AND vertigo_duration IS long AND tendency_to_fall IS left AND spontaneous_nystagmus IS right AND unterberger_test IS positive_left THEN Neuritis_vestibularis IS likely;
RULE 2 : IF rotary_vertigo IS sudden AND vertigo_duration IS long AND tendency_to_fall IS right AND spontaneous_nystagmus IS left AND unterberger_test IS positive_right THEN Neuritis_vestibularis IS likely;
RULE 3 : IF rotary_vertigo IS attack AND vertigo_duration IS medium AND tendency_to_fall IS left AND spontaneous_nystagmus IS right AND unterberger_test IS positive_left THEN Neuritis_vestibularis IS possible;
RULE 4 : IF rotary_vertigo IS attack AND vertigo_duration IS medium AND tendency_to_fall IS right AND spontaneous_nystagmus IS left AND unterberger_test IS positive_right THEN Neuritis_vestibularis IS possible;
RULE 5 : IF rotary_vertigo IS sudden AND vertigo_duration IS long AND tendency_to_fall IS left AND spontaneous_nystagmus IS right AND unterberger_test IS not_executed THEN Neuritis_vestibularis IS possible;
RULE 6 : IF rotary_vertigo IS sudden AND vertigo_duration IS long AND tendency_to_fall IS right AND spontaneous_nystagmus IS left AND unterberger_test IS not_executed THEN Neuritis_vestibularis IS possible;
RULE 7 : IF rotary_vertigo IS constant OR vertigo_duration IS short OR unterberger_test IS negative THEN Neuritis_vestibularis IS unlikely; 

END_RULEBLOCK

END_FUNCTION_BLOCK